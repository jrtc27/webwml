#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité de déni de service à distance a été découverte dans
graphicsmagick, une collection d’outils de traitement d’images et leurs
bibliothèques associées.</p>

<p>Un fichier contrefait pour l'occasion peut être utilisé pour produire un
dépassement de tampon basé sur le tas et un plantage d'application en exploitant
un défaut dans la fonction AcquireCacheNexus dans magick/pixel_cache.c.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.3.16-1.1+deb7u14.</p>

<p>Nous vous recommendons de mettre à niveau vos paquets graphicsmagick.
<p><b>Remarque</b> : le paquet graphicsmagick précédant introduisait par
inadvertance une dépendance à liblcms2-2. Cette version de paquet réutilise
liblcms1. Si votre système ne requiert pas autre part liblcms2-2, vous pouvez
envisager de le supprimer en suivant la mise à niveau de graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1168.data"
# $Id: $

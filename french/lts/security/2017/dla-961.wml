#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7650">CVE-2017-7650</a>

<p>Des filtres basés sur les ACL peuvent être contournés par des clients réglant
leur identifiant nom_utilisateur/client à « # » ou « + ». Cela permet à des
clients connectés localement ou à distance d’accéder à des sujets MQTT dont ils
doivent avoir les droits. Le même problème peut être présent dans les greffons
de contrôle d’authentification d’accès de tierce partie pour Mosquitto.</p>

<p>La vulnérabilité se produit uniquement quand les filtres basés sur les ACL
sont utilisés ou, éventuellement, lorsque les greffons de tierce partie sont
utilisés.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.15-2+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mosquitto.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-961.data"
# $Id: $

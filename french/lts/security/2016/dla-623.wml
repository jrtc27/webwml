#use wml::debian::translation-check translation="0e9af03d8cf68ff5ddec5d25a056c8dacce03437" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Dawid Golunski de LegalHackers a découvert que la version de Tomcat 7 de
Debian était vulnérable à une élévation locale de privilèges. Des
attaquants locaux qui ont obtenu l'accès au serveur dans le contexte de
l'utilisateur de tomcat7, grâce à une vulnérabilité dans une application
web, était capable de remplacer un fichier par un lien symbolique vers un
fichier arbitraire.</p>

<p>L'annonce complète peut être consultée à l'adresse</p>

<p><url "http://legalhackers.com/advisories/Tomcat-Debian-based-Root-Privilege-Escalation-Exploit.txt"></p>

<p>En complément cette mise à jour de sécurité corrige aussi le bogue
nº 821391 de Debian. Le nom du propriétaire du fichier dans /etc/tomcat7 ne
sera plus forcé sans condition lors de la mise à niveau. Comme précaution
supplémentaire, les permissions de fichier des fichiers de configuration
spécifiques à Debian dans /etc/tomcat7 sont passées à 640 pour interdire un
accès en lecture pour tous.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 7.0.28-4+deb7u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-623.data"
# $Id: $

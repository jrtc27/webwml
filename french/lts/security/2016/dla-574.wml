#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans qemu-kvm, une solution
complète de virtualisation sur les machines x86.

Le projet « Common Vulnerabilities and Exposures » (CVE) identifie les
problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5239">CVE-2015-5239</a>

<p>Lian Yihan a découvert que QEMU gérait incorrectement certaines charges
utiles de message dans le pilote d'affichage de VNC. Un client malveillant
pourrait utiliser ce problème pour provoquer le blocage du processus de
QEMU, avec pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2857">CVE-2016-2857</a>

<p>Ling Liu a découvert que QEMU gérait incorrectement les routines de
sommes de contrôle IP. Un attaquant dans le client pourrait utiliser ce
problème pour provoquer le plantage de QEMU, avec pour conséquence un déni
de service, ou éventuellement la divulgation d'octets de la mémoire de
l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4020">CVE-2016-4020</a>

<p>Donghai Zdh a découvert que QEMU gérait incorrectement le TPR (Task
Priority Register). Un attaquant privilégié dans le client pourrait
utiliser ce problème pour éventuellement la divulgation d'octets de la
mémoire de l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4439">CVE-2016-4439</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-6351">CVE-2016-6351</a>

<p>Li Qiang a découvert que l'émulation du contrôleur Fast SCSI
(FSC) 53C9X est affectée par des problèmes d'accès hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5403">CVE-2016-5403</a>

<p>Zhenhao Hong a découvert qu'un administrateur client malveillant peut
provoquer une allocation de mémoire illimitée dans QEMU (ce qui peut
provoquer une condition d'épuisement de mémoire) en soumettant des requêtes
virtio sans prendre la peine d'attendre leur achèvement.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.1.2+dfsg-6+deb7u14.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu-kvm.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-574.data"
# $Id: $

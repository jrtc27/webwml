#use wml::debian::translation-check translation="6ddaddfb0236085b4e8b5e26e1eedc9e2123de80" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème a été découvert dans lighttpd, un serveur web rapide ayant
une empreinte mémoire minimale.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19052">CVE-2018-19052</a>:

<p>Un problème a été découvert dans mod_alias_physical_handler dans
mod_alias.c dans les versions de lighttpd antérieures à 1.4.50. Il y a une
traversée potentielle du répertoire ../ d'un répertoire unique au-dessus
de la cible de l'alias, avec une configuration particulière de mod_alias où
l'alias correspondant ne possède pas de caractère « / » final, mais où le
chemin du système de fichiers de la cible de l'alias possède lui un
caractère « / » final.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1.4.45-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lighttpd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de lighttpd, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/lighttpd">\
https://security-tracker.debian.org/tracker/lighttpd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2887.data"
# $Id: $

#use wml::debian::translation-check translation="a71a84a576c7d1407c36b955371bd71e1613c6d1" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Matthias Gerstner a découvert que l'option --join de Firejail, un bac à
sable pour restreindre un environnement d'application, était vulnérable à
une élévation locale de privilèges vers ceux du superutilisateur.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
0.9.58.2-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firejail.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firejail, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firejail">\
https://security-tracker.debian.org/tracker/firejail</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3061.data"
# $Id: $

#use wml::debian::translation-check translation="b285fdd5d0db6413fe22f4075a2079a7c7a94067" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans jetty, un moteur de
servlets Java et un serveur web. Un attaquant peut divulguer des accréditations
chiffrées telles que les mots de passe à un utilisateur local, divulguer des
chemins d’installation, détourner des sessions d’utilisateur ou trafiquer des
applications web colocalisées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9735">CVE-2017-9735</a>

<p>Jetty est prédisposé à un canal temporel caché (timing channel) dans
util/security/Password.java qui facilite pour les attaquants distants
l’obtention d’accès en observant les durées écoulées avant le refus de
mots de passe incorrects.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12536">CVE-2018-12536</a>

<p>Sur des applications web déployées qui utilisent la gestion d’erreur standard,
quand une requête intentionnellement mauvaise arrive ne correspondant pas à un
modèle d’URL dynamique et finalement gérée par le service de fichier statique de
DefaultServlet, les mauvais caractères peuvent déclencher un
java.nio.file.InvalidPathException qui inclut le chemin entier vers le
répertoire de base des ressources que DefaultServlet ou l’application web
utilise. Si ce InvalidPathException est alors pris en charge par le
gestionnaire d’erreur par défaut, le message InvalidPathException est inclus
dans la réponse d’erreur, révélant le chemin complet du serveur au système
faisant la requête.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10241">CVE-2019-10241</a>
<p>Le serveur est vulnérable à des conditions de script intersite si un client
distant utilise un URL spécialement formaté pour les DefaultServlet ou
ResourceHandler configurés pour montrer une énumération de contenu de
répertoires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10247">CVE-2019-10247</a>
<p>La combinaison du serveur en cours sur n’importe quel système d’exploitation
et la version de Jetty peut révéler l’emplacement du répertoire pleinement
qualifié des ressources de base sur la sortie d’une erreur 404 due à l’absence
de contexte correspondant au chemin demandé. Le comportement par défaut du
serveur pour jetty-distribution et jetty-home inclura à la fin de l’arbre
Handler un DefaultHandler, qui est responsable du rapport de cette erreur 404, et
présentera les divers contextes configurés en HTML pour que les utilisateurs
cliquent dessus. Cet HTML produit inclut une sortie qui contient l’emplacement
du répertoire pleinement qualifié des ressources de base pour chaque
contexte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27216">CVE-2020-27216</a>

<p>Sur les systèmes tel Unix, le répertoire temporaire du système est partagé
entre tous les utilisateurs de ce système. Un utilisateur colocalisé peut
observer le processus de création d’un sous-répertoire temporaire dans le
répertoire temporaire partagé et concourir pour compléter la création du
sous-répertoire temporaire. Si l’attaquant est le plus rapide, alors il aura
les permissions de lecture et écriture dans les sous-répertoires utilisés pour
décompresser les applications web, y compris leurs fichiers jar WEB-INF/lib
et JSP. Si n’importe quel code est exécuté en dehors de ce répertoire
temporaire, cela peut conduire à une vulnérabilité d’élévation locale des
privilèges.</p></li>

</ul>

<p>Cette mise à jour inclut aussi plusieurs autres corrections de bogues et
améliorations. Pour plus d’informations, veuillez vous référer au fichier
changelog de l’amont.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 9.2.30-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jetty9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jetty9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/jetty9">\
https://security-tracker.debian.org/tracker/jetty9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2661.data"
# $Id: $

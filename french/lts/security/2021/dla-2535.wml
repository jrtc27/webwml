#use wml::debian::translation-check translation="2b2ea5e286ac88ca8fc739244d1d66819b78f8d5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans ansible, un
système de gestion de configuration, de déploiement et d’exécution de tâche.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7481">CVE-2017-7481</a>

<p>Ansible échoue à marquer correctement les résultats du greffon de recherche
comme non sûrs. Si un attaquant peut contrôler le résultat d’appels lookup(), il
pourrait injecter des chaînes Unicode pour une analyse par le système de patrons
jinja2, aboutissant à une exécution de code. Par défaut, le langage de patrons
jinja2 est désormais noté comme <q>non sûr</q> et n’est pas évalué.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10156">CVE-2019-10156</a>

<p>Un défaut a été découvert dans la façon dont les patrons sont implémentés
dans Ansible, entrainant une possibilité de divulgation d'informations à travers
une substitution inattendue de variable. En tirant parti de cette substitution
inattendue, le contenu de n’importe quelle variable peut être divulgué.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14846">CVE-2019-14846</a>

<p>Ansible journalisait au niveau DEBUG. Cela conduisait à une divulgation des
accréditations si un greffon utilisait une bibliothèque qui journalisait les
accréditations au niveau DEBUG. Ce défaut n’affecte pas les modules d’Ansible
car ils sont exécutés dans un processus séparé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14904">CVE-2019-14904</a>

<p>Un défaut a été découvert dans le module solaris_zone de la Communauté
d’Ansible. Lors de la définition du nom pour la zone sur l’hôte Solaris, ce nom
est vérifié en listant le processus avec la simple commande <q>ps</q> sur la
machine distante. Un attaquant pourrait exploiter ce défaut en contrefaisant le
nom de la zone et en exécutant des commandes arbitraires sur l’hôte distant.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.2.1.0-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ansible.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ansible, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ansible">https://security-tracker.debian.org/tracker/ansible</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2535.data"
# $Id: $

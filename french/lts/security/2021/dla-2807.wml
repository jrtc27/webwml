#use wml::debian::translation-check translation="bd1c41626be9a3684a2bbfcb86f5b55e03bb38bc" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25219">CVE-2021-25219</a>

<p>Kishore Kumar Kothapalli a découvert que le cache du serveur lame dans
BIND, une implémentation de serveur DNS, peut être malmené par un attaquant
pour dégrader de façon significative les performances du résolveur, avec
pour conséquence un déni de service (longs délais de réponse aux requêtes
du client et arrêt de résolution DNS sur les hôtes du client).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5740">CVE-2018-5740</a>

<p>« deny-answer-aliases » est une fonction peu utilisée destinée a aider
les opérateurs de serveurs récursifs à protéger les utilisateurs finaux
d'attaques par rattachement DNS, une méthode potentielle pour contourner le
modèle de sécurité utilisé par les navigateurs client. Néanmoins, un défaut
dans cette fonction facilite, lorsqu'elle est active, la survenue d'un
échec d'assertion dans name.c.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1:9.10.3.dfsg.P4-12.3+deb9u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2807.data"
# $Id: $

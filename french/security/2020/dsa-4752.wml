#use wml::debian::translation-check translation="5346ad19e1bb39a2123f70e49de6fe4ffa9caa5b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans BIND, une
implémentation de serveur DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8619">CVE-2020-8619</a>

<p>Un caractère astérisque dans un nœud non terminal vide peut provoquer un
échec d'assertion, avec pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8622">CVE-2020-8622</a>

<p>Dave Feldman, Jeff Warren et Joel Cunningham ont signalé qu'une réponse
TSIG tronquée peut conduire à un échec d'assertion, avec pour conséquence
un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8623">CVE-2020-8623</a>

<p>Lyu Chiy a signalé qu'un défaut dans le code natif de PKCS#11 peut
conduire à un échec d'assertion déclenchable à distance, avec pour
conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8624">CVE-2020-8624</a>

<p>Joop Boonen a signalé que les règles d'update-policy de type
<q>subdomain</q> sont appliquées incorrectement, permettant des mises à jour
de toutes les composantes de la zone en même temps que du sous-domaine
prévu.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1:9.11.5.P4+dfsg-5.1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4752.data"
# $Id: $

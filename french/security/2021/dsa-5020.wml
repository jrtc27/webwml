#use wml::debian::translation-check translation="9d48707cc1410dc9bc5673e6b67304a923f19557" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Chen Zhaojun de l'équipe Alibaba Cloud Security a découvert une
vulnérabilité de sécurité critique dans Log4j d'Apache, une infrastructure de
journalisation répandue pour Java. La fonction JNDI, utilisée dans la
configuration, les messages de journal et les paramètres, ne protégeait pas
d'un LDAP et d'autres points de terminaison liés à JNDI contrôlés par un
attaquant. Un attaquant qui peut contrôler les messages ou les paramètres
de messages de journal, peut exécuter du code arbitraire chargé à partir de
serveurs LDAP quand la recherche/substitution de messages est active. Ce
comportement a été désactivé par défaut à partir de la version  2.15.0.</p>

<p>Cette mise à jour corrige également le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-9488">\
CVE-2020-9488</a> dans la distribution oldstable (Buster). Une validation
incorrecte de certificat avec erreur d'hôte dans le flux (« appender »)
SMTP de log4j d'Apache. Cela pourrait permettre l'interception d'une
connexion SMTPS par une attaque de type homme du milieu qui pourrait
aboutir à une divulgation de n'importe quel message de journal envoyé au
moyen de cet « appender ».</p>

<p>Pour la distribution oldstable (Buster), ce problème a été corrigé dans
la version 2.15.0-1~deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 2.15.0-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache-log4j2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache-log4j2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/apache-log4j2">\
https://security-tracker.debian.org/tracker/apache-log4j2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5020.data"
# $Id: $

#use wml::debian::translation-check translation="1b62dcf348569f7b3d0975c674a776e714dc31fc" maintainer="Giuseppe Sacco"
#use wml::debian::template title="Derivate Debian"

<p>
C'è un gran <a href="#list">numero di distribuzioni</a> basate su Debian.
Alcuni potrebbero voler dar loro un'occhiata <em>dopo</em> aver visto i rilasci ufficiali Debian.
</p>

<p>
Una derivata da Debian è una distribuzione basata sul lavoro fatto in Debian,
ma che ha una propria identità, un proprio scopo e uno specifico pubblico, ed è
creata da un'entità indipendente da Debian.
Le derivate modificano Debian in modo da raggiungere lo scopo che si sono prefissate.
</p>

<p>
Debian incoraggia le organizzazioni che vogliono sviluppare nuove distribuzioni
basate su Debian. Secondo lo spirito del <a href="$(HOME)/social_contract">contratto
sociale</a>, speriamo che le derivate possano contribuire al lavoro in Debian e
nei progetti a monte in modo che tutti possano beneficiare delle loro migliorie.
</p>

<h2 id="list">Quali derivate sono disponibili?</h2>

<p>
Vorremmo evidenziare le seguenti derivate di Debian:
</p>

## Please keep this list sorted alphabetically
## Please only add derivatives that meet the criteria below
<ul>
    <li>
      <a href="https://grml.org/">Grml</a>:
      sistema live per amministratori di sistema.
      <a href="https://wiki.debian.org/Derivatives/Census/Grml">Informazioni aggiuntive</a>.
    </li>
    <li>
      <a href="https://www.kali.org/">Kali Linux</a>:
      audit di sicurezza e test di penetrazione.
      <a href="https://wiki.debian.org/Derivatives/Census/Kali">Informazioni aggiuntive</a>.
    </li>
    <li>
      <a href="https://pureos.net/">Purism PureOS</a>:
	  <a href="https://www.fsf.org/news/fsf-adds-pureos-to-list-of-endorsed-gnu-linux-distributions-1">FSF-endorsed</a>
	  rilascio continuo, specifico per riservatezza, sicurezza e utilità.
      <a href="https://wiki.debian.org/Derivatives/Census/Purism">Informazioni aggiuntive</a>.
    </li>
    <li>
      <a href="https://tails.boum.org/">Tails</a>:
      mantenere riservatezza e anonimato.
      <a href="https://wiki.debian.org/Derivatives/Census/Tails">Informazioni aggiuntive</a>.
    </li>
    <li>
      <a href="https://www.ubuntu.com/">Ubuntu</a>:
      diffondere Linux in tutto il mondo.
      <a href="https://wiki.debian.org/Derivatives/Census/Ubuntu">Informazioni aggiuntive</a>.
    </li>
</ul>

<p>
Le distribuzioni basate su Debian sono anche elencate alla pagina
<a href="https://wiki.debian.org/Derivatives/Census">Censimento delle derivate da Debian</a>
oltre che in <a href="https://wiki.debian.org/Derivatives#Lists">altri posti</a>.
</p>

<h2>Perché usare una derivata al posto di Debian?</h2>

<p>
Se si hanno necessità specifiche che sono gestite meglio da una derivata,
si potrebbe preferirla al posto di Debian.
</p>

<p>
Se si fa parte di una comunità specifica o un gruppo di persone per le quali
c'è una derivata, si può preferire si usarla al posto di Debian.
</p>

<h2>Perché Debian ha interesse nelle derivate?</h2>

<p>
Le derivate portano Debian ad un pubblico maggiore arrivando 
dove ci sono requisiti o esperienze che altrimenti non raggiungeremmo.
Sviluppando relazioni con le derivate,
<a href="https://wiki.debian.org/Derivatives/Integration">integrando</a>
informazioni su di esse nell'infrastruttura Debian e facendo nostre
le loro modifiche, condividiamo la nostra esperienza con le derivate,
espandiamo quanto conosciamo delle nostre derivate e del loro pubblico,
espandiamo potenzialmente la comunità Debian,
miglioriamo Debian per il nostro pubblico attuale e
rendiamo Debian adeguata a ulteriore pubblico.
</p>

<h2>Quali derivate Debian vengono menzionate da Debian?</h2>

## Examples of these criteria are in the accompanying README.txt
<p>
Le derivate menzionate qui sopra soddisfano questi criteri:
</p>

<ul>
    <li>cooperano attivamente con Debian</li>
    <li>sono mantenute attivamente</li>
    <li>sono gestite da un gruppo nel quale ci sia almeno un membro di Debian</li>
    <li>hanno partecipato al censimento delle derivate di Debian e hanno incluso un sources.list nella loro pagina di censimento</li>
    <li>hanno una caratterista o un focus che le contraddistingue</li>
    <li>sono distribuzioni note e affermate</li>
</ul>

<h2>Perché derivare Debian?</h2>

<p>
Può essere più veloce modificare una distribuzione esistente, come Debian, rispetto a farne
una iniziando da zero, questo perché il formato dei pacchetti, i «repository», i pacchetti
base e altre cose sono già definiti e usabili.
Molto software è già pacchettizzato sicché non è necessario spendere tempo per molte cose.
Questo permette alle derivate di focalizzarsi sui bisogni uno specifico pubblico.
</p>

<p>
Debian assicura che  quanto distruiamo è <a href="$(HOME)/intro/free">libero</a>
perché le derivate possano modificarlo e ridistruirlo al loro pubblico.
Lo facciamo controllando la licenze del software che distribuiamo
rispetto alle <a href="$(HOME)/social_contract#guidelines">linee guida del software libero in Debian (DFSG)</a>.
</p>

<p>
Debian offre diversi cicli di <a href="$(HOME)/releases/">rilascio</a> che sono
disponibili perché le derivate ci basino la loro distribuzione.
Questo permette alla derivate di provare software
<a href="https://wiki.debian.org/DebianExperimental">sperimentale</a>,
muoversi <a href="$(HOME)/releases/unstable/">velocemente</a>,
aggiornare <a href="$(HOME)/releases/testing/">spesso</a> con qualità assicurata,
avere una <a href="$(HOME)/releases/stable/">base solida</a> per il loro lavoro,
usare <a href="https://backports.debian.org/">nuovo</a> software su una base solida,
sfruttare il supporto per la <a href="$(HOME)/security/">sicurezza</a>
compreso quello <a href="https://wiki.debian.org/LTS">esteso</a>.
</p>

<p>
Debian supporta numerose di diverse <a href="$(HOME)/ports/">architetture</a>
e contributori stanno <a href="https://wiki.debian.org/DebianBootstrap">lavorando</a>
su metodi per crearne automaticamente su nuovi tipi di processori.
Questo permette alle derivate di usare l'hardware che preferiscono
o supportare nuovi processori.
</p>

<p>
La comunità Debian e persone che operano su derivate già esistenti sono
disponibili ad aiutare chi voglia creare nuove distribuzioni.
</p>

<p>
Le derivate sono create per numerosi motivi, quali
la traduzione in nuove lingue,
supporto per hardware specifici,
meccanismi di installazione diversi o
supporto ad una particolare comunità o gruppo di persone.
</p>

<h2>Come derivare da Debian?</h2>

<p>
Le derivate possono usare una parte dell'infrastruttura Debian, se necessario
(come i «repository»).
Le derivae dovrebbero cambiare i riferimenti a Debian (come il logo, il nome,
eccetera) e ai servizi Debian (come il sito web o il BTS).
</p>

<p>
Se lo scopo è di definire degli insiemi di pacchetti da installare,
un modo interessate di procedere facendolo in Debian è
creare un <a href="$(HOME)/blends/">blend Debian</a>.
</p>

<p>
Informazioni dettagliate sullo sviluppo sono disponibili nelle
<a href="https://wiki.debian.org/Derivatives/Guidelines">linee guida</a>,
altre indicazioni possono essere reperite all'<a
href="https://wiki.debian.org/DerivativesFrontDesk">accoglienza delle derivate («front desk»)</a>.
</p>

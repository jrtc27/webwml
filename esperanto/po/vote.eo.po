msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 16:59+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr ""

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr ""

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr ""

#: ../../english/template/debian/votebar.wml:22
#, fuzzy
msgid "Nominations"
msgstr "Donacoj"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr ""

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr ""

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr ""

#: ../../english/template/debian/votebar.wml:34
#, fuzzy
msgid "Proposer"
msgstr "proponita"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal G Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal H Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal G Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal H Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr ""

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr ""

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr ""

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr ""

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr ""

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr ""

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr ""

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr ""

#: ../../english/template/debian/votebar.wml:112
#, fuzzy
msgid "Proposal G"
msgstr "proponita"

#: ../../english/template/debian/votebar.wml:115
#, fuzzy
msgid "Proposal H"
msgstr "proponita"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr ""

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr ""

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr ""

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr ""

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr ""

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr ""

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr ""

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr ""

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr ""

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr ""

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr ""

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr ""

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr ""

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr ""

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr ""

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr ""

#: ../../english/template/debian/votebar.wml:172
#, fuzzy
msgid "Minimum Discussion"
msgstr "Diskutata"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr ""

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr ""

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr ""

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Esperanta&nbsp;je&nbsp;Aŭspiciantoj"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "Diskutata"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Voĉdono&nbsp;Malfermita"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Decidita"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr ""

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Alia"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Hejma&nbsp;Voĉdona&nbsp;Paĝo"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Kiel&nbsp;Fari"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Sendi&nbsp;Proponon"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Modifi&nbsp;Proponon"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Sekvi&nbsp;Proponon"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Legi&nbsp;Rezulton"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Voĉdono"

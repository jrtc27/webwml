#use wml::debian::template title="Inhalts-Aushandlung"
#use wml::debian::translation-check translation="c646e774c01abc2ee3d32c65d6920ea4f03159dc"
# $Id$
# Translator: Noel Köthe, noel@koethe.net, 2001-07-18
# Updated: Holger Wansing <hwansing@mailbox.org>, 2020.

<h3>Woher weiß der Server, welche Datei er versenden soll</h3>

<p>Sie werden merken, dass interne Links nicht mit .html enden.
Das liegt daran, dass der Server Inhalts-Aushandlung (Content-Negotiation)
benutzt, um zu entscheiden, welche Version des Dokuments er ausliefern soll.
Wenn es mehr als eine Möglichkeit gibt, wird der Server eine
Liste aller möglichen auslieferbarer Dateien erstellen, z.B. wenn
die Anfrage nach "about" ist, dann könnte die Liste von
Vervollständigungen "about.en.html" und "about.de.html" sein.
Der Standard für Debian-Server ist, die englischen Seiten
auszuliefern, aber dies ist konfigurierbar.</p>

<p>Wenn ein Client die entsprechende Variable gesetzt hat, z.B. um
Deutsch auszuliefern, dann wird in dem Beispiel von oben "about.de.html"
ausgeliefert. Das schöne an dieser Einstellung ist, dass eine andere Sprache
zurückgegeben wird, wenn die verlangte Sprache nicht verfügbar ist (was
hoffentlich besser ist als nichts). Die Entscheidung, welche Seite
verschickt wird, ist ein bisschen verwirrend, doch anstatt es hier zu 
beschreiben, sollten Sie die verbindliche Antwort von
<a href="https://httpd.apache.org/docs/current/content-negotiation.html">
https://httpd.apache.org/docs/current/content-negotiation.html</a>
lesen, wenn Sie daran interessiert sind.</p>

<p>Weil viele Benutzer nichts von der Existenz der Inhalts-Aushandlung
wissen, gibt es Links am Fuß von jeder Seite, die direkt zu den Versionen
in anderen Sprachen führt. Dies wird durch ein
Perl-Skript berechnet, das von wml aufgerufen wird, wenn die Seite 
erzeugt wird.</p>

<p>Es gibt auch eine Möglichkeit, die Spracheinstellung des Browsers mittels
eines Cookies zu überschreiben; dadurch wird eine bestimmte Sprache gegenüber
derjenigen, die Ihr Browser als bevorzugt ansieht, vorgezogen.</p>

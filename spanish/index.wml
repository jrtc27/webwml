#use wml::debian::links.tags
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/index.def"
#use wml::debian::mainpage title="<motto>"
#use wml::debian::translation-check translation="0ee4ac09253cee5e3e22c2621453da29a0526449" maintainer="Laura Arjona Reina"

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<div id="splash">
  <h1>Debian</h1>
</div>

<!-- The first row of columns on the site. -->
<div class="row">
  <div class="column column-left">
    <div style="text-align: center">
      <h1>La comunidad</h1>
      <h2>¡Debian es una comunidad!</h2>

#include "$(ENGLISHDIR)/index.inc"

      <div class="row">
      <div class="community column">
        <a href="intro/people" aria-hidden="true">
          <img src="Pics/users.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/people">Las personas</a></h2>
        <p>Quiénes somos y qué hacemos</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/philosophy" aria-hidden="true">
          <img src="Pics/heartbeat.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/philosophy">Nuestra filosofía</a></h2>
        <p>Por qué lo hacemos y cómo lo hacemos</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="devel/join/" aria-hidden="true">
          <img src="Pics/user-plus.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="devel/join/">Implíquese, contribuya</a></h2>
        <p>¡Cómo puede unirse!</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#community" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#community">Más...</a></h2>
        <p>Información adicional sobre la comunidad Debian</p>
      </div>
    </div>
  </div>
  <div class="column column-right">
    <div style="text-align: center">
      <h1>El sistema operativo</h1>
      <h2>¡Debian es un sistema operativo completamente libre!</h2>
      <div class="os-img-container">
        <img src="Pics/debian-logo-1024x576.png" alt="Debian">
        <a href="$(HOME)/download" class="os-dl-btn"><download></a>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/why_debian" aria-hidden="true">
          <img src="Pics/trophy.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/why_debian">Por qué Debian</a></h2>
        <p>Qué hace a Debian especial</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="support" aria-hidden="true">
          <img src="Pics/life-ring.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="support">Soporte a usuarios</a></h2>
        <p>Obtener ayuda y documentación</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="security/" aria-hidden="true">
          <img src="Pics/security.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="security/">Actualizaciones de seguridad</a></h2>
        <p>Avisos de seguridad de Debian (DSA)</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#software" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#software">Más...</a></h2>
        <p>Más enlaces a descargas y acerca del software</p>
      </div>
    </div>
  </div>
</div>

<hr>
<!-- An optional row highlighting events happening now, such as releases, point releases, debconf and minidebconfs, and elections (dpl, GRs...). -->
<!--<div class="row">
 <div class="column styled-href-blue column-left">
   <div style="text-align: center">
     <h2>¡<a href="https://debconf22.debconf.org/">DebConf22</a> está en marcha!</h2>
     <p>La Conferencia de Debian se está celebrando en Prizren, Kosovo desde el domingo 17 al domingo 24 de julio de 2022.</p>
    </div>
  </div>
</div>-->

<!-- The next row of columns on the site. -->
<!-- The News will be selected by the press team. -->


<div class="row">
  <div class="column styled-href-blue column-left">
    <div style="text-align: center">
      <h1><projectnews></h1>
      <h2>Noticias y anuncios acerca de Debian</h2>
    </div>

    <:= get_top_news() :>

    <!-- No more News entries behind this line! -->
    <div class="project-news">
      <div class="end-of-list-arrow"></div>
      <div class="project-news-content project-news-content-end">
        <a href="News">Todas las noticias</a> &emsp;&emsp;
	<a class="rss_logo" style="float: none" href="News/news">RSS</a>
      </div>
    </div>
  </div>
</div>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Noticias de Debian" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Noticias del proyecto Debian" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Avisos de seguridad de Debian (solo títulos)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Avisos de seguridad de Debian (resúmenes)" href="security/dsa-long">
:#rss#}


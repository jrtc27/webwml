#use wml::debian::cdimage title="Descarga de imágenes de CD/DVD de Debian mediante HTTP/FTP" BARETITLE=true
#use wml::debian::translation-check translation="f4fe84f1063f34ac4bfb75eb963e6fa71ba0e642" maintainer="Laura Arjona Reina"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<div class="tip">
<p><strong>Por favor, no descargue imágenes de CD o DVD con su navegador web
de la misma forma que descarga otros archivos</strong>. La razón es que
si fracasa la descarga, la mayoría de los navegadores no le permiten
recuperarla desde el punto en el que falló.</p>
</div>

<p>En su lugar, utilice una herramienta que permita la continuación,
típicamente llamada <q>gestor de descargas</q>. Hay muchos complementos de navegadores
que hacen esta tarea, o quizá prefiera instalar un programa independiente.
En Linux/Unix, puede usar <a
href="http://aria2.sourceforge.net/">aria2</a>, <a
href="http://dfast.sourceforge.net/">wxDownload Fast</a> o (en
línea de órdenes) <q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q> o
<q><tt>curl&nbsp;-C&nbsp;-&nbsp;</tt><em>URL</em></q>. Hay
muchas más opciones descritas en esta <a
href="https://es.wikipedia.org/wiki/Anexo:Comparaci%C3%B3n_de_gestores_de_descargas">comparación
de gestores de descarga</a>.</p>

<p>Dispone para descarga directa de las siguientes imágenes:</p>

<ul>

  <li><a href="#stable">Imágenes oficiales de CD/DVD de la versión <q>estable</q></a></li>

  <li><a href="#firmware">Imágenes <b>no oficiales</b> de CD/DVD de la versión <q>estable</q> con
  «firmware» <b>no libre</b> incluido</a></li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Imágenes oficiales de CD/DVD de la distribución <q>en pruebas («testing»)</q> (<em>se
  generan semanalmente</em>)</a></li>

<comment>
  <li>Imágenes no oficiales de CD/DVD de las distribuciones <q>en pruebas</q> e <q>inestable</q>
  gracias a fsn://HU; <a href="#unofficial">vea más adelante</a>.</li>
</comment>

</ul>

<p>Véase también:</p>
<ul>

  <li>Una <a href="#mirrors">lista completa de réplicas de <tt>debian-cd/</tt></a></li>

  <li>Para imágenes de <q>instalación por red</q> (150-300&nbsp;MB),
  mire la página de <a href="../netinst/">instalación por red</a>.</li>
  
  <li>Para imágenes de <q>instalación por red</q> de <q>pruebas («testing»)</q>,
  mire la página del <a href="$(DEVEL)/debian-installer/">instalador de Debian</a>.</li>
</ul>

<hr />

<h2><a name="stable">Imágenes oficiales de CD/DVD de la versión <q>estable</q></a></h2>

<p>Para instalar una máquina sin conexión a Internet, se pueden usar las imágenes
de CD (700&nbsp;MB cada una) o de DVD (4.7&nbsp;GB cada una).
Descargue la primera imagen de CD o DVD, grábela usando una grabadora de CD o DVD 
(o en una memoria USB en las versiones i386 y amd64) y reinicie desde ella.</p>

<p>El <strong>primer</strong> disco CD/DVD contiene todos los archivos necesarios 
para instalar un sistema Debian estándar.<br />
Para evitar descargas innecesarias, por favor, <strong>no</strong> descargue 
otra imagen de CD o DVD a menos que sepa que necesita los paquetes 
que contiene.</p>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>

<p>Los siguientes enlaces apuntan a archivos de imágenes que ocupan hasta 
700&nbsp;MB, haciéndolos adecuados para grabarse en CD-R(W) normales:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>

<p>Los siguientes enlaces apuntan a archivos de imágenes que ocupan hasta 4.7&nbsp;GB, 
haciéndolas adecuadas para grabarse en DVD-R/DVD+R normales y similares:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Asegúrese de echarle un vistazo a la documentación antes de instalar.
<strong>Si solo va a leer un documento</strong> antes de instalar, lea nuestro
<a href="$(HOME)/releases/stable/i386/apa">Cómo instalar</a>, un paseo rápido
por el proceso de instalación. Entre otra documentación útil están:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Guía de instalación</a>,
    las instrucciones de instalación detalladas</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Documentación del instalador de Debian</a>
    incluyendo las respuestas a preguntas frecuentes</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Erratas del instalador de Debian</a>,
    la lista de problemas conocidos en el instalador</li>
</ul>

<hr />

# Traductores: el siguiente párrafo existe (igual o de manera similar) varias veces en webwml,
# así que por favor intentemos mantener las traducciones consistentes. Vean:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">Imágenes no oficiales de CD/DVD con «firmware» no libre incluido</a></h2>

<div id="firmware_nonfree" class="important">
<p>
Si algún componente hardware de su sistema <strong>requiere cargar «firmware»
no libre</strong> con el controlador de dispositivo, puede usar uno de los
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
archivos comprimidos con paquetes de «firmware» común</a> o descargar una imagen <strong>no oficial</strong>
que incluya estos «firmwares» <strong>no libres</strong>. En la <a href="../../releases/stable/amd64/ch06s04">guía de instalación</a> puede encontrar
instrucciones sobre cómo usar los archivos comprimidos e información general sobre cómo cargar
el «firmware» durante la instalación.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">Imágenes no oficiales
de instalación de <q>estable</q> con «firmware» incluido</a>
</p>
</div>

<hr />

<h2><a name="mirrors">Réplicas registradas del archivo de <q>debian-cd</q></a></h2>

<p>Fíjese en que <strong>algunas réplicas no están actualizadas</strong> &mdash;
antes de descargarla, ¡verifique que el
número de la versión de las imágenes es el mismo que el listado
<a href="../#latest">en este sitio</a>!.
Además, tenga en  cuenta de que algunos sitios no copian el conjunto de imágenes
completo (especialmente las imágenes de DVD) debido a su tamaño.</p>

<p><strong>Si duda, use la <a href="https://cdimage.debian.org/debian-cd/">imagen 
del CD del servidor principal</a> en Suecia.</strong> o pruebe
<a href="http://debian-cd.debian.net/">el selector automático de réplica
(experimental)</a> que le redirigirá automáticamente a una réplica cercana
que tenga la versión actual.</p>

<p>¿Está interesado en ofrecer las imágenes de CD de Debian en
su réplica? Si es así, vea las <a href="../mirroring/">instrucciones acerca de 
cómo configurar una réplica de imágenes de CD de Debian</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"

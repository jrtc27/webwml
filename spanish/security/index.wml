#use wml::debian::template title="Información sobre seguridad" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="bf3aeb5742a500bf1fedb987fefa8a01abba100c"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Cómo mantener seguro su sistema Debian</a></li>
<li><a href="#DSAS">Avisos recientes</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian se toma la seguridad muy en serio. Nos hacemos cargo de todos los problemas de seguridad que reclaman nuestra atención y los corregimos en un plazo razonable.</p>
</aside>

<p>
La experiencia ha demostrado que <q>la seguridad por medio de la oscuridad</q> nunca funciona. La revelación pública permite encontrar soluciones mejores, y en un menor plazo de tiempo, a los problemas de seguridad. Esta página muestra la situación de Debian respecto a diversos agujeros de seguridad conocidos que podrían, potencialmente, afectar al sistema operativo Debian.</p>
</p>

<p>
El proyecto Debian coordina muchos avisos de seguridad con otros agentes del software libre, con el resultado de que estos avisos se publican el mismo día que se hace pública la vulnerabilidad.
</p>
  
# "reasonable timeframe" might be too vague, but we don't have 
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.


<p>
Debian también participa en los esfuerzos de estandarización de seguridad:
</p>

<ul>
  <li>Los <a href="#DSAS">avisos de seguridad de Debian</a> son <a href="cve-compatibility">compatibles con CVE</a> (revise las <a href="crossreferences">referencias cruzadas</a>).</li>
  <li>Debian está representado en el foro del proyecto <a href="https://oval.cisecurity.org/">Open Vulnerability Assessment Language</a>
# Nota: se trata de un lenguaje de programación, no de un idioma
(n.t. Lenguaje abierto de detección de vulnerabilidades).</li>
</ul>

<h2><a id="keeping-secure">Cómo mantener seguro su sistema Debian</a></h2>


<p>
Para recibir los últimos avisos de seguridad de Debian, suscríbase a la lista de correo <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>

<p>
Además, puede usar <a href="https://packages.debian.org/stable/admin/apt">APT</a> para obtener fácilmente las últimas actualizaciones de seguridad. Para mantener el sistema operativo Debian actualizado en cuanto a parches de seguridad, añada la línea siguiente en el fichero <code>/etc/apt/sources.list</code>:
</p>

<pre>
deb&nbsp;http://security.debian.org/debian-security&nbsp;<current_release_security_name>&nbsp;main&nbsp;contrib&nbsp;non-free
</pre>

<p>
Tras grabar los cambios, ejecute las dos órdenes siguientes para descargar e instalar las actualizaciones pendientes:
</p>

<pre>
apt-get update &amp;&amp; apt-get upgrade
</pre> 

<p>
El archivo de seguridad está firmado con las <a href="https://ftp-master.debian.org/keys.html">claves del archivo Debian</a> usuales.
</p>

<p>
Para más información sobre cuestiones de seguridad en Debian, diríjase a nuestras FAQ y a nuestra documentación:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">Preguntas frecuentes sobre la seguridad</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Seguridad en Debian</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Avisos recientes</a></h2>

<p>Estas páginas web incluyen un archivo condensado de los avisos de
seguridad enviados a la lista
<a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a>.

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Avisos de seguridad de Debian (solo títulos)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Avisos de seguridad de Debian (resúmenes)" href="dsa-long">
:#rss#}

<p>
Los últimos avisos de seguridad de Debian están disponibles como <a href="dsa">ficheros RDF</a>. Asimismo, ofrecemos una <a href="dsa-long">versión ligeramente más larga</a> de los ficheros, que incluye el primer párrafo del aviso correspondiente. De esta manera puede identificar fácilmente de qué trata.
</p>

#include "$(ENGLISHDIR)/security/index.include"
<p>También se encuentran disponibles los avisos de seguridad anteriores:
<:= get_past_sec_list(); :>

<p>Las distribuciones de Debian no son vulnerables a todos los problemas de seguridad. El
<a href="https://security-tracker.debian.org/">sistema de seguimiento de problemas de seguridad de Debian</a> 
recolecta toda la información sobre el estado de vulnerabilidad de los paquetes de Debian.
Permite hacer búsquedas por nombre de CVE o por paquete.</p>

#use wml::debian::template title="Debian Installer" NOHEADER="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6f892e2bb4879338aa7540b6e970f730a9674781"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<h1>Nyheder</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">&AElig;ldre nyheder</a></p>


<h1>Installering med Debian Installer</h1>

<p>
<if-stable-release release="bullseye">
<strong>For officielle installeringsmedier og oplysninger om Debian
<current_release_bullseye></strong>, se
<a href="$(HOME)/releases/bullseye/debian-installer">siden om bullseye</a>.
</if-stable-release>
<if-stable-release release="bookworm">
<strong>For officielle installeringsmedier og oplysninger om Debian
<current_release_bookworm></strong>, se
<a href="$(HOME)/releases/bookworm/debian-installer">siden om bookworm</a>.
</if-stable-release>
</p>

<div class="tip">
<p>Alle aftryk der linkes til herunder, er til versionen af Debian Installer, 
som udvikles til den næste udgave af Debian, og den vil som standard installere 
Debian testing (<q><current_testing_name></q>).</p>
</div>

<!-- Shown in the beginning of the release cycle: no Alpha/Beta/RC released yet. -->
<if-testing-installer released="no">

<p><strong>For at installere Debian testing</strong>, anbefales det at du 
anvender installeringsprogrammets <strong>daglige opbygninger</strong>.  
Følgende aftryk er tilgængelige som daglige opbygninger:</p>

</if-testing-installer>

<!-- Shown later in the release cycle: Alpha/Beta/RC available, point at the latest one. -->
<if-testing-installer released="yes">

<p><strong>For at installere Debian testing</strong>, anbefales det at du 
anvender <strong><humanversion /></strong>-udgaven af installeringsprogrammet, 
efter at have kigget på dens <a href="errata">fejlside</a>.  Følgende aftryk 
er tilgængelige med <humanversion />:</p>

<h2>Officiel udgave</h2>

<div class="line">

<div class="item col50">
<strong>cd-aftryk med netinst (generelt 180-450 MB)</strong>
<netinst-images />
</div>

</div>

<div class="line">

<div class="item col50">
<strong>komplette cd-sæt</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>komplette dvd-sæt</strong>
<full-dvd-images />
</div>

</div>

<h2>Aktuelle ugentlige øjebliksbilleder</h2>

<div class="line">

<div class="item col50">
<strong>Cd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>Dvd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">

<div class="item col50">
<strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>andre aftryk (netboot, USB-pind, osv.)</strong>
<other-images />
</div>

</div>

<p>Eller installer det <strong>aktuelle ugentlige øjebliksbillede af Debian 
testing</strong>, der anvender den samme version af installeringsprogrammet,
som den seneste udgivelse:</p>

<div class="line">

<div class="item col50">
<strong>komplette cd-sæt</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>komplette dvd-sæt</strong>
<devel-full-dvd-images />
</div>

</div>

<div class="line">

<div class="item col50">
<strong>Cd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>Dvd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>

</div>

<div class="line">

<div class="item col50">
<strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>

</div>

<p>Ønsker du at anvende det sidste nye, enten for at hjælpe os med at teste en 
kommende udgave af installeringsprogrammet eller på grund af hardwareproblemer 
eller andre problemer, så prøv et af de <strong>dagligt opbyggede aftryk</strong>, 
der indeholder den seneste tilgænglige version af installeringsprogrammets 
komponenter.</p>

</if-testing-installer>

<h2>Aktuelt dagligt øjebliksbillede</h2>

<div class="line">

<div class="item col50">
<strong>cd-aftryk med netinst (generelt 150-280 mb)<!-- og visitkort 
(generelt 20-50 mb)--></strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>netinst-cd-aftryk (gennem 
<!-- <strong>netinst- og visitkorts-cd-aftryk (gennem -->
<a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo/>
</div>

</div>

<div class="line">

<div class="item col50">
<strong>netinst-flerarkitektur-cd-aftryk</strong>
<devel-multi-arch-cd />
</div>

<div class="item col50 lastcol">
<strong>andre aftryk (netboot, USB-pind, etc.)</strong>
<devel-other-images />
</div>

</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<div id="firmware_nonfree" class="important">

<p>Hvis noget af hardwaren i dit system <strong>kræver at ikke-fri firmware skal 
indlæses</strong> sammen med enhedsdriveren, kan du anvende en 
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
tarball med almindelige firmwarepakker</a> eller hente et 
<strong>uofficielt</strong> aftryk, som indehodler denne 
<strong>ikke-frie</strong> firmware.  Vejledning i hvordan en tarball anvendes 
og generelle oplysninger om indlæsning af firmware under en installering, finder 
man i <a href="../../releases/stable/amd64/ch06s04">Installation Guide</a>.</p>

<p><a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">\
uofficielle installeringsaftryk til udgaven <q>stable</q> med medfølgende firmware</a></p>

<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/daily-builds/sid_d-i/current/">\
uofficielle aftryk med medfølgende firmware - daglige opbygninger</a>
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/weekly-builds/">\
uofficielle aftryk med medfølgende firmware - ugentlige opbygninger</a>
</p>
</div>

<hr />

<p><strong>Bemærkninger</strong></p>

<ul>
#    <li>
#	Før du henter de dagligt opbyggede aftryk, foreslår vi at du undersøger 
#	om der er <a href="https://wiki.debian.org/DebianInstaller/Today">kendte 
#        problemer</a>.
#    </li>

    <li>
	En arkitektur kan (midlertidigt) være fjernet fra oversigten over 
	dagligt opbyggede aftryk, hvis de daglige aftryk ikke (med sikkerhed) er 
        tilgængelige.
    </li>

    <li>
	Til installeringsaftrykkene, er verifikationsfilerne 
	(<tt>SHA512SUMS</tt>, <tt>SHA256SUMS</tt> og andre) tilgængelige i de 
	samme mapper som aftrykkene.
    </li>

    <li>
	Til hentning af komplette cd- og dvd-aftryk anbefales det at anvende 
	jigdo.
    </li>

    <li>
	Kun et begrænset antal aftryk af dvd-sættene er tilgængelige for 
	direkte download.  De fleste brugere har ikke behov for al softwaren, 
	der er tilgængelig på skiverne, derfor, for at spare plads på 
	downloadserverne og filspejlene, er de komplette sæt kun tilgængelige 
	via jidgo.
    </li>

    <li>
	<em>Netinst cd</em>-aftryk med flere arkitekturer understøtter 
	i386/amd64; installeringen svarer til at installere fra en 
	enkelt arkitekturs netinst-aftryk.
    </li>
</ul>

<p><strong>Efter at have anvendt Debian Installer</strong>, bedes du sende os en
<a href="https://d-i.debian.org/manual/da.amd64/ch05s04.html#submit-bug">\
installeringsrapport på engelsk</a>, også selv om der ikke var problemer.</p>


<h1>Dokumentation</h1>

<p><strong>Læser du kun ét dokument</strong> før du installerer, så læs vores
<a href="https://d-i.debian.org/manual/da.amd64/apa.html">\
Installerings-HOWTO</a>, som er en hurtig gennemgang af installeringsprocessen. 
Andre nyttige dokumenter er blandt andre:</p>

<ul>
    <li>
	Installeringsvejledning: 
#	<a href="$(HOME)/releases/stable/installmanual">version 
#	til den aktuelle udgave</a> &ndash;
	<a href="$(HOME)/releases/testing/installmanual">udviklingsversion (testing)</a> &ndash;
	<a href="https://d-i.debian.org/manual/">seneste version (Git)</a>
	<br />
	indeholder en udførlig installeringsvejledning
    </li>
    <li>
	<a href="https://wiki.debian.org/DebianInstaller/FAQ">OSS om Debian 
	Installer</a> og <a href="$(HOME)/CD/faq/">OSS om Debian-cd'er</a><br />
	ofte stillede spørgsmål med svar
    </li>
    <li>
	<a href="https://wiki.debian.org/DebianInstaller">Debian Installers 
	wiki</a><br /> fællesskabsvedligeholdt dokumentation
    </li>
</ul>


<h1>Kontakt</h1>

<p><a href="https://lists.debian.org/debian-boot/">Postlisten debian-boot</a> er
det primære engelsksprogede forum til diskussioner og arbejde på Debian 
Installer.</p>

<p>Vi har også en engelsksproget IRC-kanal, #debian-boot på <tt>irc.debian.org</tt>.  
Kanalen anvendes hovedsagligt til udvikling, men ind i mellem også til support.
Modtager du ikke et svar, så prøv i stedet postlisten.</p>

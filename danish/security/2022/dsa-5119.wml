#use wml::debian::translation-check translation="db3c16a066355af630de96e011541fd523a7b343" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder blev opdaget i Subversion, et versionsstyringssystem.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28544">CVE-2021-28544</a>

    <p>Evgeny Kotkov rapporterede at Subversion-servere afslører 
    <q>copyfrom</q>-stier, der burde være skjulte ifølge de opsatte stibaserede 
    autorisationsregler (authz).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24070">CVE-2022-24070</a>

    <p>Thomas Weissschuh rapporterede at Subversions mod_dav_svn var sårbar over 
    for en anvendelse efter frigivelse, når der blev slået stibaserede 
    autorisationsregler op, hvilket kunne medføre lammelsesangreb (nedbrud af 
    HTTPD-worker'en som håndterer forespørgslen).</p></li>

</ul>

<p>I den gamle stabile distribution (buster), er disse problemer rettet
i version 1.10.4-1+deb10u3.</p>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 1.14.1-3+deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine subversion-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende subversion, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/subversion">\
https://security-tracker.debian.org/tracker/subversion</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5119.data"

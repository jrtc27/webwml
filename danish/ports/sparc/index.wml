#use wml::debian::template title="SPARC-tilpasning" NOHEADER="yes"
#use wml::debian::translation-check translation="f9d5abd797e762089776545824869e3e44bd2c42"
#include "$(ENGLISHDIR)/ports/sparc/menu.inc"

<h1>Debians SPARC-tilpasning</h1>

<ul>
  <li><a href="#intro">Oversigt</a></li>
  <li><a href="#status">Aktuel status</a></li>
  <li><a href="#sparc64bit">Om understøttelse af 64 bit-SPARC</a> 
     <ul>
       <li><a href="#kernelsun4u">Oversættelse af kerner til sun4u</a>
     </ul></li>
  <li><a href="#errata">Fejl</a></li>
  <li><a href="#who">Hvem er vi?  Hvordan kan jeg hjælp til?</a></li>
  <li><a href="#links">Hvor kan jeg få flere oplysninger?</a></li>
</ul>


<h2 id="intro">Oversigt</h2>

<p>Disse sider er tænkt som en hjælp til brugere og Debian-udviklere som kører 
Debian GNU/Linux på SPARC-arkitekturen  Her kan de finde oplysninger om den 
aktuelle status, kendte problemer, oplysninger til og om dem der tilpasser 
Debian, samt henvisninger til hvor man kan få flere oplysninger.</p>


<h2 id="status">Aktuel status</h2>

<p>Understøttelse af præ-UltraSPARC-maskiner blev droppet med ophøret af 
Debian Etch-support (se <a href="https://wiki.debian.org/Sparc32">\
+https://wiki.debian.org/Sparc32</a>).  Derefter krævede 32 bit-tilpasningen en 
UltraSPARC-CPU og kørte en 64 bit-kernel.  Denne 32 bit-tilpasning blev senere 
droppet, med ophøret af Debian Wheezy-support.</p>

<p>I øjeblikket har Debian ingen officiel SPARC-tilpasning, men en komplet 
64 bit-SPARC-tilpasning, kaldet sparc64, er understøttet af Debian 
Ports-holdet.</p>


<h2 id="sparc64bit">Om understøttelse af 64 bit-SPARC</h2>

<p>Debians SPARC-tilpasning, som nævnt ovenfor, understøtter ikke 
arkitekturene sun4u (<q>Ultra</q>) og sun4v (Niagara CPU).  Den bruger en 64 
bit-kerne (oversat med gcc 3.3 eller nyere), men de fleste programmer kører 32 
bit. Dette kaldes også et <q>32 bit-userland</q>.</p>

<p>Arbejdet med at tilpasse Debian til SPARC 64 (også kendt som, <q>UltraLinux</q>)
anses for tiden ikke for at være et fuldstændigt tilpasningsarbejde som 
andre tilpasninger. Snarere er formålet at være en <em>tilføjelse</em> 
(<q>add-on</q>) til <a href="../sparc/">Debians SPARC-tilpasning</a>.</p>

<p>Faktisk er der ikke rigtigt nogen idé i, at lade alle programmer køre i
64 bit-tilstand.  Fuldstændig 64 bit-tilstand involverer et betydeligt 
overhead (hukommelse og harddiskplads), ofte uden nogen fordele. Nogle
programmer kan have fordel af at være i 64 bit-tilstand, og det er formålet
med dette tilpasningsarbejde.</p>


<h3 id="kernelsun4u">Oversættelse af kerner til sun4u</h3>

<p>Du skal bruge Linux 2.2-kildekodetræet eller nyere, for at oversætte 
Linux-kernen til sun4u.</p>

<p>Vi anbefaler kraftigt at du også anvender pakken <tt>kernel-package</tt> som 
en hjælp til installering og administration af kerner. Du kan oversætte en 
konfigureret kerne på én gang med følgende kommando (som root):</p>

<pre>
  make-kpkg --subarch=sun4u --arch_in_name --revision=custom.1 kernel_image
</pre>


<h2 id="errata">Fejl</h2>

<p>Nogle af de hyppige problemer med rettelser eller omgåelser findes på vores
<a href="problems">fejlside</a>.</p>


<h2 id="who">Hvem er vi?  Hvordan kan jeg hjælpe til?</h2>

<p>Debians SPARC-tilpasning er som Debian blevet til ved en distribueret 
indsats. Utallige mennesker har hjulpet til med tilpasningen og 
dokumentationen, selv om en kort <a href="credits">takkeliste</a> er 
tilgængelig.</p>

<p>Hvis du vil hjælp til, så tilmeld dig postlisten 
&lt;debian-sparc@lists.debian.org&gt;, som 
<a href="#links">beskrevet nedenfor</a> og deltag i debatten.</p>

<p>Registrerede udviklere som aktivt vil tilpasse og uploade tilpassede pakker
bør læse tilpasningsretningslinjerne i <a href="$(DOC)/developers-reference/">\
Udviklernes opslagsbog</a>, og se <a href="porting">\
SPARC-tilpasningsside</a>.</p>


<h2 id="links">Hvor kan jeg få flere oplysninger?</h2>

<p>Der er en Debian Wiki-side specifikt om 
<a href="https://wiki.debian.org/Sparc64">Debian Sparc64-tilpasnikngen</a>.</p>

<p>Det bedste sted at stille Debian-specifikke spørgsmål om SPARC-tilpasnignen
er på postlisten, <a href="https://lists.debian.org/debian-sparc/">\
&lt;debian-sparc@lists.debian.org&gt;</a>.
<a href="https://lists.debian.org/debian-sparc/">Postlistearkivet</a> kan
læses ved hjælp af en browser.</p>

<p>For at tegne abonnement på listen sendes en e-mail til
<a href="mailto:debian-sparc-request@lists.debian.org">\
debian-sparc-request@lists.debian.org</a>, med ordet <q>subscribe</q> i 
emnelinjen, og ingen tekst i e-mailens krop.  Alternativt kan man tegne 
abonnement via WWW ved hjælp af <a href="https://lists.debian.org/debian-sparc/">\
postliste-abonnementssiden</a>.</p>

<p>Spørgsmål vedrørende kernen stilles på engelsk på postlisten
&lt;sparclinux@vger.rutgers.edu&gt;.  Tegn abonnement ved at sende en e-mail 
indeholdende <q>subscribe sparclinux</q> i e-mailens krop, til adressen 
<a href="mailto:majordomo@vger.rutgers.edu">majordomo@vger.rutgers.edu</a>.
Der findes selvfølgelig også en Red Hat-liste.</p>

#use wml::debian::translation-check translation="96c05360e385187167f1ccbce37d38ce2e5e6920" maintainer="Seunghun Han (kkamagui)"
<define-tag pagetitle>데비안 10 업데이트: 10.3 출시</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>데비안 프로젝트는 데비안 <release> (codename <q><codename></q>) 안정(stable)
배포판의 세번째 업데이트가 나왔다는 것을 알려드리게 되어 기쁘게 생각합니다.
이 포인트(point) 릴리스는 몇몇 심각한 문제의 조치 및 보안 이슈와 관련된 수정을 주로
담고 있습니다.
보안 권고는 이미 개별적으로 공개되었고 활용 가능한 곳에서 참조될 수 있습니다.</p>

<p>포인트 릴리스는 데비안 <release>의 새 버전을 만드는 것이 아니며, 포함된 일부
패키지만 업데이트 한다는 것을 주의하세요.
이전 버전의 <q><codename></q> 미디어를 버릴 필요 없습니다. 설치 후, 최신 데비안
미러를 이용하여 패키지를 현재 버전으로 업그레이드 할 수 있거든요.</p>

<p>security.debian.org의 업데이트를 자주 설치하는 사람들은 패키지를 많이
업데이트하지 않아도 되며, 해당 업데이트는 대부분 포인트 릴리스에 포함되어
있습니다.</p>

<p>신규 설치 이미지는 정규 위치(다운로드 페이지, ftp 서버 등)에 곧 공개될
겁니다.</p>

<p>패키지 관리 시스템이 수많은 데비안 HTTP 미러 중 하나를 가리키게 해서 기존
설치를 이 개정판으로 업그레이드할 수 있습니다.
포괄적인 미러 서버 목록은 아래에 있습니다.</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>다양한 종류의 버그 수정</h2>

<p>이 버전의 안정(stable) 업데이트는 아래 패키지에 몇몇 중요한 수정을 했습니다.</p>

<table border=0>
<tr><th>패키지</th>               <th>수정 이유</th></tr>
<correction alot "테스트 세트 키(Key)의 만료시간 제거 및 빌드 오류 수정">
<correction atril "문서가 로딩되지 않았을 때 발생하는 세그먼트 폴트 오류 수정 및 초기화되지 않은 메모리 읽기 오류 수정 [CVE-2019-11459]">
<correction base-files "포인트 릴리스용 업데이트">
<correction beagle "JAR 파일의 심볼 링크 대신 랩퍼 스크립트 제공 및 재활성화">
<correction bgpdump "세그먼트 폴트 오류 수정">
<correction boost1.67 "libboost-numpy의 오류를 일으키는 미정의 행위 수정">
<correction brightd "/sys/class/power_supply/AC/online를 실제로 읽어서 값을 <q>0</q>과 비교">
<correction casacore-data-jplde "테이블을 2040개까지 포함">
<correction clamav "새로운 업스트림 릴리스, 서비스 거부 오류 [CVE-2019-15961] 수정, ScanOnAccess 옵션 제거, clamonacc로 교체">
<correction compactheader "썬더버드 68 호환을 위한 새로운 업스트림 릴리스">
<correction console-common "파일이 포함되지 않아서 발생한 회귀 오류 수정">
<correction csh "eval의 세그먼트 폴트 오류 수정">
<correction cups "ppdOpen의 메모리 유출 오류 수정 및 ippSetValuetag에 기본 언어 검증 오류 수정 [CVE-2019-2228]">
<correction cyrus-imapd "cyrus-upgrade-db에 백업 타입 추가 및 업그레이드 이슈 수정">
<correction debian-edu-config "WPAD가 접근 불가일 때 프록시 설정 유지">
<correction debian-installer "제안된 업데이트(proposed-updates)용으로 다시 빌드, ARM용 mini.iso 일부 수정 및 EFI netboot 기능 구동, 로컬 빌드를 하는 사용자를 돕기 위해 USE_UDEBS_FROM을 불안정(unstable)에서 버스터(buster)로 업데이트">
<correction debian-installer-netboot-images "제안된 업데이트(proposed-updates)용으로 다시 빌드">
<correction debian-security-support "여러 패키지의 보안 지원 상태 업데이트">
<correction debos "최신 golang-github-go-debos-fakemachine용으로 다시 빌드">
<correction dispmua "썬더버드 68 호환을 위한 새로운 업스트림 릴리스">
<correction dkimpy "새로운 업스트림 안정(stable) 릴리스">
<correction dkimpy-milter "시작 시 권한 관리 부분 수정 및 Unix 소켓 동작">
<correction dpdk "새로운 업스트림 안정(stable) 릴리스">
<correction e2fsprogs "e2fsck의 잠재적 스택 언더플로우 수정 [CVE-2019-5188] 및 e2fsck의 use-after-free 오류 수정">
<correction fig2dev "다수의 ^A로 끝나는 Fig v2 문자열 허용 [CVE-2019-19555], 정수 오버플로우를 야기하는 거대 화살표 타입 거부 [CVE-2019-19746], 몇몇 오류 수정 [CVE-2019-19797]">
<correction freerdp2 "realloc 반환값 처리 오류 수정 [CVE-2019-17177]">
<correction freetds "UDT가 8종이 되도록 수정 [CVE-2019-13508]">
<correction git-lfs "신규 Go 버전과 빌드 시 오류 수정">
<correction gnubg "프로그램 시작 동안 메시지를 쌓는데 사용되는 정적 버퍼 크기 증가 및 스페인어 번역이 버퍼를 넘어가지 않도록 수정">
<correction gnutls28 "gnutls 2.x와 연계 문제 수정 및 RegisteredID를 이용한 인증서 파싱 오류 수정">
<correction gtk2-engines-murrine "다른 테마와 상호 설치 오류 문제 해결">
<correction guile-2.2 "빌드 오류 해결">
<correction libburn "<q>cdrskin의 복수 트랙 굽기가 느리고 트랙 1에서 멈추는 문제</q> 해결">
<correction libcgns "ppc64el에서 빌드 오류 해결">
<correction libimobiledevice "일부 SSL 쓰기 문제 해결">
<correction libmatroska "1.4.7에 새로운 심볼이 추가되어 공유 라이브러리 의존성을 1.4.7으로 올림">
<correction libmysofa "[CVE-2019-16091 CVE-2019-16092 CVE-2019-16093 CVE-2019-16094 CVE-2019-16095] 보안 오류 수정">
<correction libole-storage-lite-perl "2020년 이후의 연도 변환 문제 해결">
<correction libparse-win32registry-perl "2020년 이후의 연도 변환 문제 해결">
<correction libperl4-corelibs-perl "2020년 이후의 연도 변환 문제 해결">
<correction libsolv "힙 버퍼 오버플로우 수정 [CVE-2019-20387]">
<correction libspreadsheet-wright-perl "이전에 미사용된 OpenDocument 스프레드시트와 JSON 변환 옵션 전송 문제 수정">
<correction libtimedate-perl "2020년 이후의 연도 변환 문제 해결">
<correction libvirt "pygrub을 실행할 수 있도록 Apparmor 수정, osxsave 사용 불가, QEMU 커맨드 라인에 ospke 추가. 이러한 수정은 virt-install로 생성된 몇몇 설정과 함께 신규 QEMU를 도움">
<correction libvncserver "RFBserver에서 원격지로 스택 메모리 정보를 유출하지 않도록 수정 [CVE-2019-15681], 멀티 스레드로 동작하는 VNC 서버에서 접속이 종료되거나 세그먼트 폴트 오류가 발생하여 멈추는 오류 수정, VMWare 서버와 접속하는 문제 해결, vncviewer가 접속할 때 x11vnc의 오류 문제 해결">
<correction limnoria "Math 플러그인에서 원격지로 정보를 노출하거나 원격 코드 실행이 가능한 오류 해결 [CVE-2019-19010]">
<correction linux "새로운 업스트림 안정(stable) 릴리스">
<correction linux-latest "4.19.0-8 리눅스 커널 ABI용 업데이트">
<correction linux-signed-amd64 "새로운 업스트림 안정(stable) 릴리스">
<correction linux-signed-arm64 "새로운 업스트림 안정(stable) 릴리스">
<correction linux-signed-i386 "새로운 업스트림 안정(stable) 릴리스">
<correction mariadb-10.3 "새로운 업스트림 안정(stable) 릴리스 [CVE-2019-2938 CVE-2019-2974 CVE-2020-2574]">
<correction mesa "shmget()를 0777 대신 0600 권한으로 호출 [CVE-2019-5068]">
<correction mnemosyne "PIL 관련 빠진 의존성 추가">
<correction modsecurity "쿠키 헤더 파싱 오류 수정 [CVE-2019-19886]">
<correction node-handlebars "<q>helperMissing</q>와 <q>blockHelperMissing</q>를 직접 호출하지 못하도록 수정 [CVE-2019-19919]">
<correction node-kind-of "ctorName()의 타입 검사 취약점 수정 [CVE-2019-20149]">
<correction ntpsec "느린 DNS 재시도 관련 수정, if-up hook 수정을 위해 ntpdate -s (syslog) 수정, 문서 관련 수정">
<correction numix-gtk-theme "다른 테마와 상호 설치 오류 문제 해결">
<correction nvidia-graphics-drivers-legacy-340xx "새로운 업스트림 안정(stable) 릴리스">
<correction nyancat "nyancat-server용 systemd 유닛 추가를 위해 깨끗한 환경에서 다시 빌드">
<correction openjpeg2 "힙 오버플로우 [CVE-2018-21010]와 정수 오버플로우 [CVE-2018-20847] 수정">
<correction opensmtpd "이전 버전에서 smtpd.conf 문법을 변경하는 사람을 위한 경고 추가, smtpctl와 setgid와 opensmtpq 설치, 설정 과정에서 hostname이 0 아닌 오류값 반환 시 처리 추가">
<correction openssh "seccomp 샌드박스에서 ipc 거부, OpenSSL 1.1.1d와 몇몇 아키텍처의 Linux &lt; 3.19에서 발생하는 오류 해결">
<correction php-horde "Horde Cloud Block에서 크로스 사이트 스크립트 저장 관련 이슈 수정 [CVE-2019-12095]">
<correction php-horde-text-filter "유효하지 않은 정규 표현식 수정">
<correction postfix "새로운 업스트림 안정(stable) 릴리스">
<correction postgresql-11 "새로운 업스트림 안정(stable) 릴리스">
<correction print-manager "CPUS가 다수의 프린트 작업과 관련해서 같은 ID를 반환할 때 발생하는 오류 수정">
<correction proftpd-dfsg "CRL 이슈 수정 [CVE-2019-19270 CVE-2019-19269]">
<correction pykaraoke "폰트 경로 수정">
<correction python-evtx "<q>hexdump</q> 연동 수정">
<correction python-internetarchive "해시 추출 후 파일을 닫아 파일 디스크립터가 고갈되지 않도록 수정">
<correction python3.7 "보안 이슈 수정 [CVE-2019-9740 CVE-2019-9947 CVE-2019-9948 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935]">
<correction qtbase-opensource-src "PPD 모드가 아닌 프린터 지원 및 PPD 지원을 위해 멈추는 문제 해결, QLabel에 리치 텍스트를 입력했을 때 죽는 문제 해결, 그래픽 태블릿의 호버 이벤트 문제 해결">
<correction qtwebengine-opensource-src "PDF 파싱 문제 해결, 실행가능한 스택 비활성화">
<correction quassel "설정이 저장되었을 때 quasselcore의 AppArmor 거부 수정, 데비안의 기본 채널 수정, 불필요한 NEWS 파일 제거">
<correction qwinff "비정상 파일을 탐지하여 죽는 문제 해결">
<correction raspi3-firmware "커널 5.x에서 시리얼 콘솔 탐지 문제 해결">
<correction ros-ros-comm "보안 이슈 해결 [CVE-2019-13566 CVE-2019-13465 CVE-2019-13445]">
<correction roundcube "새로운 업스트림 안정(stable) 릴리스, enigma 플러그인의 안전하지 않은 설정 수정 [CVE-2018-1000071]">
<correction schleuder "<q>보호된 헤더</q>와 제목이 없는 메일에서 키워드 인식하는 문제 해결, 키(key)를 가져오거나 갱신할 때 자가 서명 아닌 부분 제거, `refresh_keys`에 전달된 인자가 기존 리스트에 없을 때 오류 처리, 관리자에게 알림 메일을 보낼 때 빠졌던 List-Id 헤더 추가, 복호화 문제를 우아하게 처리, ASCII-8BIT 인코딩을 기본으로 설정">
<correction simplesamlphp "PHP 7.3와 비호환 문제 해결">
<correction sogo-connector "썬더버드 68 호환을 위한 새로운 업스트림 릴리스">
<correction spf-engine "Unix 소켓 동작을 위해 시작 시 권한 관리 문제 해결, TestOnly용 문서 업데이트">
<correction sudo "pwfeedback이 활성화되고 입력이 tty가 아닐 때 버퍼 오버플로우 문제 해결, 참고로 버스터(buster)에서 익스플로잇 불가 [CVE-2019-18634]">
<correction systemd "fs.file-max sysctl을 ULONG_MAX 보다 LONG_MAX로 설정, 정적인 사용자를 위해 실행 디렉터리의 /mode 소유권 수정, 서비스 시작 전에 CacheDirectory나 StateDirectory 같은 실행 디렉터리가 User= 에 기록된 사용자로 적절히 소유자가 변경되도록 보장">
<correction tifffile "랩퍼 스크립트 수정">
<correction tigervnc "보안 이슈 수정 [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "보안 이슈 수정 [CVE-2014-6053 CVE-2019-8287 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction uif "nftables로 이관하는 관점에서 ip(6)tables-restore로 경로 수정">
<correction unhide "스택 고갈 문제 수정">
<correction x2goclient "SCP 모드일 때 ~/와 ~user{,/}와 ${HOME}{,/}와 $HOME{,/}를 목적지 경로에서 제거, CVE-2019-14889 수정 사항이 적용된 신규 libssh 버전에서 회귀 오류 해결">
<correction xmltooling "로딩 과정에서 중단을 일으킬 수 있는 경쟁 조건 해결">
</table>


<h2>보안 업데이트</h2>


<p>이 개정판은 아래의 보안 업데이트를 안정(stable) 릴리스에 추가합니다.
보안팀은 각 업데이트에 대해서 이미 권고사항을 공개했습니다.</p>

<table border=0>
<tr><th>권고 ID</th>  <th>패키지</th></tr>
<dsa 2019 4546 openjdk-11>
<dsa 2019 4563 webkit2gtk>
<dsa 2019 4564 linux>
<dsa 2019 4564 linux-signed-i386>
<dsa 2019 4564 linux-signed-arm64>
<dsa 2019 4564 linux-signed-amd64>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4566 qemu>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4570 mosquitto>
<dsa 2019 4571 enigmail>
<dsa 2019 4571 thunderbird>
<dsa 2019 4572 slurm-llnl>
<dsa 2019 4573 symfony>
<dsa 2019 4575 chromium>
<dsa 2019 4577 haproxy>
<dsa 2019 4578 libvpx>
<dsa 2019 4579 nss>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4583 spip>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4586 ruby2.5>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4595 debian-lan-config>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4599 wordpress>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4605 openjdk-11>
<dsa 2020 4606 chromium>
<dsa 2020 4607 openconnect>
<dsa 2020 4608 tiff>
<dsa 2020 4609 python-apt>
<dsa 2020 4610 webkit2gtk>
<dsa 2020 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4613 libidn2>
<dsa 2020 4615 spamassassin>
</table>


<h2>제거된 패키지</h2>

<p>아래 패키지는 우리 통제를 벗어나서 제거되었습니다.</p>

<table border=0>
<tr><th>패키지</th>               <th>제거된 이유</th></tr>
<correction caml-crush "[armel] ocaml-native-compilers가 없어서 빌드 불가">
<correction firetray "현재 썬더버드 버전과 호환 불가">
<correction koji "보안 이슈">
<correction python-lamson "python-daemon의 변경으로 작동 안함">
<correction radare2 "보안 이슈, 업스트림이 더이상 안정(stable) 릴리스를 지원 안함">
<correction radare2-cutter "제거된 radare2에 의존함">

</table>

<h2>데비안 설치관리자</h2>
<p>설치 관리자는 포인트 릴리스에서 안정(stable) 릴리스와 병합된 수정 사항을
포함하도록 업데이트 되었습니다.</p>

<h2>URL</h2>

<p>개정판에서 변경된 패키지의 전체 리스트는 다음과 같습니다.</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>현재 안정(stable) 배포판:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>안정(stable) 배포판에 제안된 업데이트(proposed-update):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>안정(stable) 배포판 정보 (릴리스 노트, 정오표 등):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>보안 공지 및 정보:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>데비안에 관하여</h2>

<p>데비안 프로젝트는 완전한 자유 운영체제인 데비안을 제작하기 위해 자신의
시간과 노력을 자원하는 자유 소프트웨어 개발자의 모임입니다.</p>

<h2>연락처 정보</h2>

<p>보다 많은 정보를 원하면 <a href="$(HOME)/">https://www.debian.org/</a>에 있는
데비안 웹 페이지를 방문하거나, &lt;press@debian.org&gt;으로 이메일을 보내세요.
아니면 &lt;debian-release@lists.debian.org&gt;로 보내서 안정(stable)
릴리스 팀으로 연락하세요.</p>

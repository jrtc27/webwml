#use wml::debian::template title="Site web do Debian em diferentes idiomas" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="80696988195221adfe32e0c037530145acd35b48"

<define-tag toc-title-formatting endtag="required">%body</define-tag>
<define-tag toc-item-formatting endtag="required">[%body]</define-tag>
<define-tag toc-display-begin><p></define-tag>
<define-tag toc-display-end></p></define-tag>

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#intro">Navegação de conteúdo</a></li>
    <li><a href="#howtoset">Como definir o idioma de um navegador web</a></li>
    <li><a href="#override">Como substituir as configurações</a></li>
    <li><a href="#fix">Solução de problemas</a></li>
  </ul>
</div>

<h2><a id="intro">Navegação de conteúdo</a></h2>

<p>
Uma equipe de <a href="../devel/website/translating"> tradutores(as)</a>
trabalha no site do Debian para convertê-lo para um número crescente de idiomas
diferentes. Mas como funciona a mudança de idioma no navegador web? Um padrão
chamado <a href="$(HOME)/devel/website/content_negotiation">negociação de conteúdo</a>
permite que os(as) usuários(as) definam seus idiomas preferidos para o conteúdo
da web. A versão que eles(as) veem é negociada entre o navegador web e o
servidor web: o navegador envia as preferências ao servidor, e o servidor então
decide qual versão entregar (com base nas preferências dos(as) usuários(as) e
nas versões disponíveis).
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="http://www.w3.org/International/questions/qa-lang-priorities">Leia mais na W3C</a></button></p>

<p>
Nem todo mundo sabe sobre negociação de conteúdo, então os links na parte
inferior de cada página do Debian apontam para outras versões disponíveis.
Por favor observe que a seleção de um idioma diferente nesta lista afetará
apenas a página atual. Isso não altera o idioma padrão do seu navegador web. Se
você seguir outro link para uma página diferente, você o verá no idioma padrão
novamente.
</p>

<p>
Para alterar o idioma padrão, você tem duas opções:
</p>

<ul>
  <li><a href="#howtoset">Configure seu navegador web</a></li>
  <li><a href="#override">Substitua as preferências de idioma do seu navegador</a></li>
</ul>

<p>
Vá direto para as instruções de configuração para estes navegadores web:</p>

<toc-display />

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>O idioma original do site do
Debian é o inglês. Portanto, é uma boa idéia adicionar inglês (<code>en</code>)
no final da sua lista de idiomas. Isso funciona como um backup caso uma página
ainda não tenha sido traduzida para qualquer um dos seus idiomas preferidos.</p>
</aside>

<h2><a id="howtoset">Como definir o idioma de um navegador web</a></h2>

<p>
Antes de descrevermos como definir as configurações de idioma em diferentes
navegadores web, algumas observações gerais. Em primeiro lugar, é uma boa ideia
incluir todos os idiomas que você fala em sua lista de idiomas preferidos. Por
exemplo, se você é um falante nativo do francês, pode escolher <code>fr</code>
como seu primeiro idioma, seguido por inglês com o código de idioma <code>en</code>.
</p>

<p>
Em segundo lugar, em alguns navegadores você pode inserir códigos de idioma em
vez de escolher em um menu. Se for esse o caso, lembre-se de que criar uma lista
como <code>fr, en</code> não define sua preferência. Em vez disso, ele definirá
opções igualmente classificadas, e o servidor web pode decidir ignorar a ordem
e escolher apenas um dos idiomas. Se você deseja especificar uma preferência
real, deve trabalhar com os chamados valores de qualidade, ou seja, valores de
ponto flutuante entre 0 e 1. Um valor mais alto indica uma prioridade mais alta.
Se voltarmos ao exemplo com os idiomas francês e inglês, você pode modificar o
exemplo acima assim:
</p>

<pre>
fr; q=1.0, en; q=0.5
</pre>

<h3>Tenha cuidado com os códigos de país</h3>

<p>
Um servidor web que recebe uma solicitação de documento com o idioma preferido
<code>en-GB, fr</code> <strong>nem sempre</strong> serve a versão em inglês
antes da francesa. Isso só acontecerá se houver uma página com a extensão de
idioma <code>en-gb</code>. Mas funciona ao contrário: um servidor pode retornar
uma página <code>en-us</code> se apenas <code>en</code> estiver incluído na
lista de idiomas preferidos.
</p>

<p>
Portanto, recomendamos não adicionar códigos de país de dois campos, como
<code>en-GB</code> ou <code>en-US</code>, a menos que você tenha um bom motivo.
Se você adicionar um código de dois campos, certifique-se de incluir o código
do idioma sem a extensão também: <code>en-GB, en, fr</code>.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://httpd.apache.org/docs/current/content-negotiation.html">Mais sobre negociação de conteúdo</a></button></p>

<h3>Instruções para diferentes navegadores web</h3>

<p>
Compilamos uma lista de navegadores web populares e algumas instruções sobre
como alterar o idioma preferido para o conteúdo web em suas configurações:
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="chromium">Chrome/Chromium</toc-add-entry></strong><br>
  No canto superior direito, abra o menu e clique <em>Configurações</em> -&gt; <em>Avançado</em> -&gt; <em>Idiomas</em>. Abra o menu <em>Idioma</em> para ver a lista de idiomas. Clique nos três pontos ao lado de uma entrada para alterar a ordem. Você também pode adicionar novos idiomas, se necessário.</li>
        <li><strong><toc-add-entry name="elinks">ELinks</toc-add-entry></strong><br>
  Configurar o idioma padrão em <em>Configurar</em> -&gt; <em>Idioma</em> também mudará o idioma solicitado pelos sites. Você pode alterar esse comportamento e ajustar o <em>cabeçalho idioma-aceito</em> em <em>Configuração</em> -&gt; <em>Gerenciador de opções</em> -&gt; <em>Protocolos</em> -&gt; <em>HTTP</em></li>
        <li><strong><toc-add-entry name="epiphany">Epiphany</toc-add-entry></strong><br>
  Abra <em>Preferências</em> a partir do menu principal e mude a aba <em>Idioma</em>. Aqui você pode adicionar, remover e ordenar os idiomas.</li>
        <li><strong><toc-add-entry name="mozillafirefox">Firefox</toc-add-entry></strong><br>
  Na barra de menus no topo abra <em>Preferências</em>. Role para baixo até <em>Idiomas e aparência</em> -&gt; <em>Idioma</em> no painel <em>Geral</em> panel. Clique no botão <em>Escolher</em> para configurar o seu idioma preferido para a exibição dos sites web. Na mesma caixa de diálogo, você também pode adicionar, remover e reordenar os idiomas.</li>

        <li><strong><toc-add-entry name="ibrowse">IBrowse</toc-add-entry></strong><br>
  Vá em <em>Preferências</em> -&gt; <em>Configurações</em> -&gt; <em>Rede</em>. <em>Aceitar idioma</em> provavelmente exibe um * que é o padrão. Se você clicar no botão <em>Localização</em>, deve ser capaz de adicionar o seu idioma preferido. Se não, você pode inserir manualmente.</li>
        <li><strong><toc-add-entry name="icab">iCab</toc-add-entry></strong><br>
  <em>Editar</em> -&gt; <em>Preferências</em> -&gt; <em>Navegador</em> -&gt; <em>Fontes, Idiomas</em></li>
        <li><strong><toc-add-entry name="iceweasel">IceCat (Iceweasel)</toc-add-entry></strong><br>
  <em>Editar</em> -&gt; <em>Preferências</em> -&gt; <em>Conteúdo</em> -&gt; <em>Idiomas</em> -&gt; <em>Escolha</em></li>
        <li><strong><toc-add-entry name="ie">Internet Explorer</toc-add-entry></strong><br>
  Clique no ícone <em>Ferramentas</em>, selecione <em>Opções de internet</em>, mude para a aba <em>Geral</em> e clique no botão <em>Idiomas</em>. Clique <em>Configurar preferências de idioma</em> e no diálogo seguinte você pode adicionar, remover e reordenar os idiomas.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="konqueror">Konqueror</toc-add-entry></strong><br>
        Edite o arquivo <em>~/.kde/share/config/kio_httprc</em> e inclua a nova linha a seguir:<br>
        <code>Languages=fr;q=1.0, en;q=0.5</code></li>
        <li><strong><toc-add-entry name="lynx">Lynx</toc-add-entry></strong><br>
        Edite o arquivo <em>~/.lynxrc</em> e entre com a seguinte linha:<br>
        <code>preferred_language=fr; q=1.0, en; q=0.5</code><br>
        Como alternativa, você pode abrir as configurações do navegador pressionando [O]. Role para baixo até <em>Idioma preferido</em> e adicione a linha acima.</li>
        <li><strong><toc-add-entry name="edge">Microsoft Edge</toc-add-entry></strong><br>
        <em>Configurações e mais</em>  -&gt; <em>Configurações</em> -&gt; <em>Idiomas</em> -&gt; <em>Adicionar idiomas</em><br>
        Clique no botão de três pontos ao lado de uma entrada de idioma para mais opções e para alterar a ordem.</li>
        <li><strong><toc-add-entry name="opera">Opera</toc-add-entry></strong><br>
        <em>Configurações</em> -&gt; <em>Navegador</em> -&gt; <em>Idiomas</em> -&gt; <em>Idiomas preferidos</em></li>
        <li><strong><toc-add-entry name="safari">Safari</toc-add-entry></strong><br>
        O Safari usa as configurações de todo o sistema no macOS e iOS, portanto, para definir seu idioma preferido, por favor abra <em>Preferências do sistema</em> (macOS) ou <em>Configurações</em> (iOS)</li>
        <li><strong><toc-add-entry name="w3m">W3M</toc-add-entry></strong><br>
        Pressione [O] para abrir o <em>Painel de opções de configuração</em>, role para baixo até <em>Configurações de rede</em> -&gt; <em>Cabeçalho aceitar-idioma</em>. Pressione [Enter] para mudar as configurações (por exemplo <code>fr; q=1.0, en; q=0.5</code>) e confirme com [Enter]. Role até o fim até [OK] para salvar suas configurações.</li>
        <li><strong><toc-add-entry name="vivaldi">Vivaldi</toc-add-entry></strong><br>
        Vá em <em>Configurações</em> -&gt; <em>Geral</em> -&gt; <em>Idioma</em> -&gt; <em>Idiomas aceitos</em>, clique <em>Adicionar idioma</em> e escolhe um do menu. Use as setas para mudar a ordem de sua preferência.</li>
      </ul>
    </div>
  </div>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Embora seja sempre melhor selecionar seu idioma preferido na configuração do navegador, há uma opção para substituir as configurações com um cookie.</p>
</aside>

<h2><a id="override">Como substituir as configurações</a></h2>

<p>
Se por qualquer motivo você não conseguir definir o seu idioma preferido nas
configurações do navegador, dispositivo ou ambiente de computação, pode
substituir as preferências do seu navegador usando um cookie como último
recurso. Clique em um dos botões abaixo para colocar um único idioma no topo
da lista.
</p>

<p>
Por favor observe que isto irá configurar um
<a href="https://en.wikipedia.org/wiki/HTTP_cookie">cookie</a>. Seu navegador
apagará automaticamente o cookie se você não visitar este site por um mês.
Claro, você sempre pode excluir o cookie manualmente em seu navegador web ou
clicando no botão <em>navegador padrão</em>.
</p>

<protect pass=2>
<: print language_selector_buttons(); :>
</protect>

<h2><a id="fix">Solução de problemas</a></h2>

<p>
Às vezes, o site do Debian aparece no idioma errado, apesar de todos os
esforços para definir um idioma preferido. Nossa primeira sugestão é limpar o
cache local (disco e memória) em seu navegador antes de tentar recarregar o
site. Se você tiver certeza absoluta de que
<a href="#howtoset">configurou seu navegador</a> corretamente, então o problema
pode ser um cache corrompido ou mal configurado. Isso está se tornando um
problema sério nos dias de hoje à medida que mais e mais ISPs veem o cache
como uma forma de diminuir o tráfego na rede deles. Leia a <a href="#cache">seção</a>
sobre servidores proxy, mesmo se você achar que não está usando um.
</p>

<p>
É claro que sempre é possível que haja um problema com
<a href="https://www.debian.org/">www.debian.org</a>. Embora apenas alguns
problemas de idioma relatados nos últimos anos tenham sido causados por um bug
do nosso lado, é perfeitamente possível. Portanto, sugerimos que você primeiro
investigue suas próprias configurações e um possível problema de cache antes de
<a href="../contact">entrar em contato</a> conosco. Se
<a href="https://www.debian.org/">https://www.debian.org </a> estiver
funcionando, mas um dos <a href="https://www.debian.org/mirror/list">espelhos</a>
não, por favor relate isso para que possamos contatar os(as) mantenedores(as)
do espelho.
</p>

<h3><a name="cache">Potenciais problemas com servidores de proxy</a></h3>

<p>
Servidores proxy são, essencialmente, servidores web que não têm conteúdo
próprio. Eles ficam entre os(as) usuários(as) e os servidores web reais,
pegam as requisições de páginas web e buscam a página. Depois disso,
repassam a página a você, mas também fazem uma cópia local, que fica no cache
para requisições posteriores. Isso pode realmente baixar o tráfego de rede
quando muitos(as) usuários(as) requisitam a mesma página.</p>

<p>
Enquanto isso é uma grande ideia na maior parte do tempo, também causa falhas
quando o cache está bugado. Em particular, alguns servidores de proxy antigos
não entendem negociação de conteúdo. Isso resulta no cache de uma página em um
idioma e o provimento dela, mesmo se um idioma diferente for solicitado depois.
A única solução é atualizar ou substituir o software de cache.</p>

<p>
Historicamente, os servidores proxy eram usados apenas quando as pessoas
configuravam seus navegadores web corretamente. De qualquer modo, já não é este
o caso. Seu ISP pode estar redirecionando todas as solicitações HTTP por meio
de um proxy transparente. Se o proxy não lidar com a negociação de conteúdo
adequadamente, os(as) usuários(as) podem receber páginas em cache no idioma
errado. A única maneira de corrigir isso é reclamar com seu ISP para que ele
atualize ou substitua o software.</p>


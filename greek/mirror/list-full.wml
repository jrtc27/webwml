#use wml::debian::template title="Debian worldwide mirror sites" BARETITLE=true
#use wml::debian::translation-check translation="d83882f2dd9b20c81d0cb83b32c028d9765578c7" maintainer="galaxico"

<p>This is a <strong>complete</strong> list of mirrors of Debian.
For each site, the different types of material available are listed,
along with the access method for each type.</p>

<p>The following things are mirrored:</p>

<dl>
<dt><strong>Packages</strong></dt>
	<dd>The Debian package pool.</dd>
<dt><strong>CD Images</strong></dt>
	<dd>Official Debian CD Images. See
	<url "https://www.debian.org/CD/"> for details.</dd>
<dt><strong>Old releases</strong></dt>
	<dd>The archive of old released versions of Debian.
	<br />
	Some of the old releases also included the so-called debian-non-US
	archive, with sections for Debian packages that could not be
	distributed in the US due to software patents or use of encryption.
	The debian-non-US updates were discontinued with Debian 3.1.</dd>

</dl>

<p>The following access methods are possible:</p>

<dl>
<dt><strong>HTTP</strong></dt>
	<dd>Standard web access, but it can be used for downloading files.</dd>
<dt><strong>rsync</strong></dt>
	<dd>An efficient means of mirroring.</dd>
</dl>

<p>The authoritative copy of the following list can always be found at:
<url "https://www.debian.org/mirror/list-full">.
<br />
Everything else you want to know about Debian mirrors:
<url "https://www.debian.org/mirror/">.
</p>

<hr style="height:1">

<p>Jump directly to a country on the list:
<br />

#include "$(ENGLISHDIR)/mirror/list-full.inc"

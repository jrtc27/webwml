<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Since the release of the last Debian stable release ("stretch"),
Debian LTS ("wheezy") has been renamed <q>oldoldstable</q>, which broke
the unattended-upgrades package as described in bug #867169. Updates
would simply not be performed anymore.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.79.5+wheezy3. Note that later unattended-upgrades version released
in later Debian versions do not exhibit the same behavior, as they use
the release codename (e.g. <q>jessie</q>) instead of the suite name
(e.g. <q>oldstable</q>) in the configuration file. So later releases will
transition correctly for future LTS releases.</p>

<p>We recommend that you upgrade your unattended-upgrades packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1032.data"
# $Id: $

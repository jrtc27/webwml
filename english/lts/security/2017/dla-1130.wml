<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>This upload fixes a number of security issues in graphicsmagick.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14103">CVE-2017-14103</a>

    <p>The ReadJNGImage and ReadOneJNGImage functions in
    coders/png.c did not properly manage image pointers after certain error
    conditions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14314">CVE-2017-14314</a>

    <p>Heap-based buffer over-read in DrawDashPolygon() .</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14504">CVE-2017-14504</a>

    <p>NULL pointer dereference triggered by malformed file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14733">CVE-2017-14733</a>

    <p>Ensure we detect alpha images with too few colors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14994">CVE-2017-14994</a>

    <p>DCM_ReadNonNativeImages() can produce image list with
    no frames, resulting in null image pointer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14997">CVE-2017-14997</a>

    <p>Unsigned underflow leading to astonishingly
    large allocation request.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u10.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1130.data"
# $Id: $

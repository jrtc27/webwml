<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>lucene-solr handler supports an HTTP API (/replication?command=filecontent&amp;file=&lt;file_name&gt;)
which is vulnerable to path traversal attack. Specifically, this API does not
perform any validation of the user specified file_name parameter. This can
allow an attacker to download any file readable to Solr server process even if
it is not related to the actual Solr index state.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
3.6.0+dfsg-1+deb7u2.</p>

<p>We recommend that you upgrade your lucene-solr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1046.data"
# $Id: $

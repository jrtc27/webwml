<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9591">CVE-2016-9591</a>

      <p>Use-after-free on heap in jas_matrix_destroy
      The vulnerability exists in code responsible for re-encoding the
      decoded input image file to a JP2 image. The vulnerability is
      caused by not setting related pointers to be null after the
      pointers are freed (i.e. missing Setting-Pointer-Null operations
      after free). The vulnerability can further cause double-free.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10251">CVE-2016-10251</a>

      <p>Integer overflow in the jpc_pi_nextcprl function in jpc_t2cod.c in
      JasPer before 1.900.20 allows remote attackers to have unspecified
      impact via a crafted file, which triggers use of an uninitialized
      value.</p>

<li>Additional fix for TEMP-CVE from last upload to avoid hassle with
    SIZE_MAX</li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.900.1-13+deb7u6.</p>

<p>We recommend that you upgrade your jasper packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-920.data"
# $Id: $

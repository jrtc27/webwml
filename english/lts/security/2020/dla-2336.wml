<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Tim Starling discovered two vulnerabilities in firejail, a sandbox
program to restrict the running environment of untrusted applications.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17367">CVE-2020-17367</a>

     <p>It was reported that firejail does not respect the end-of-options
     separator ("--"), allowing an attacker with control over the command
     line options of the sandboxed application, to write data to a
     specified file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17368">CVE-2020-17368</a>

     <p>It was reported that firejail when redirecting output via --output
     or --output-stderr, concatenates all command line arguments into a
     single string that is passed to a shell. An attacker who has control
     over the command line arguments of the sandboxed application could
     take advantage of this flaw to run run arbitrary other commands.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
0.9.44.8-2+deb9u1.</p>

<p>We recommend that you upgrade your firejail packages.</p>

<p>For the detailed security status of firejail please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firejail">https://security-tracker.debian.org/tracker/firejail</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2336.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Vulnerabilities have been discovered in php5, a server-side,
HTML-embedded scripting language.  Note that this update includes a
change to the default behavior for IMAP connections.  See below for
details.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19518">CVE-2018-19518</a>

    <p>An argument injection vulnerability in imap_open() may allow a
    remote attacker to execute arbitrary OS commands on the IMAP server.</p>

    <p>The fix for the <a href="https://security-tracker.debian.org/tracker/CVE-2018-19518">CVE-2018-19518</a> vulnerability included this
    additional note from the upstream developers:</p>

    <p>Starting with 5.6.38, rsh/ssh logins are disabled by default. Use
    imap.enable_insecure_rsh if you want to enable them. Note that the
    IMAP library does not filter mailbox names before passing them to
    rsh/ssh command, thus passing untrusted data to this function with
    rsh/ssh enabled is insecure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19935">CVE-2018-19935</a>

    <p>A NULL pointer dereference leads to an application crash and a
    denial of service via an empty string in the message argument to the
    imap_mail function of ext/imap/php_imap.c.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.6.39+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1608.data"
# $Id: $

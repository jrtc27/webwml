<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Marcin <q>Icewall</q> Noga of Cisco Talos discovered an out-of-bound read
vulnerability in the CInArchive::ReadFileItem method in p7zip, a 7zr
file archiver with high compression ratio. A remote attacker can take
advantage of this flaw to cause a denial-of-service or, potentially the
execution of arbitrary code with the privileges of the user running
p7zip, if a specially crafted UDF file is processed.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
9.20.1~dfsg.1-4+deb7u2.</p>

<p>We recommend that you upgrade your p7zip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-510.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was found in PowerDNS Authoritative Server before
4.0.7 and before 4.1.7. An insufficient validation of data coming from
the user when building a HTTP request from a DNS query in the HTTP
Connector of the Remote backend, allowing a remote user to cause a
denial of service by making the server connect to an invalid endpoint,
or possibly information disclosure by making the server connect to an
internal endpoint and somehow extracting meaningful information about
the response.</p>

<p>Only installations using the pdns-backend-remote package are affected.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.4.1-4+deb8u9.</p>

<p>We recommend that you upgrade your pdns packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1737.data"
# $Id: $

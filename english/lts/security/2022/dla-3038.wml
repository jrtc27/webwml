<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>debian-security-support, the Debian security support coverage checker,
has been updated in stretch-security to mark the end of life of the
following packages:</p>

<p>* keystone:
  See <a href="https://lists.debian.org/debian-lts/2020/05/msg00011.html">https://lists.debian.org/debian-lts/2020/05/msg00011.html</a> for
  further information.</p>

<p>* libspring-java:
  See <a href="https://lists.debian.org/debian-lts/2021/12/msg00008.html">https://lists.debian.org/debian-lts/2021/12/msg00008.html</a> for
  further information.</p>

<p>* guacamole-client:
  See <a href="https://lists.debian.org/debian-lts/2022/01/msg00015.html">https://lists.debian.org/debian-lts/2022/01/msg00015.html</a> for
  further information.</p>

<p>* gpac:
  See <a href="https://lists.debian.org/debian-lts/2022/04/msg00008.html">https://lists.debian.org/debian-lts/2022/04/msg00008.html</a> for
  further information.</p>

<p>* ansible:
  Lack of an effective test suite makes proper support impossible.</p>

<p>* mysql-connector-java:
  Details of security vulnerabilities are not disclosed. MySQL has
  been replaced by MariaDB. We recommend to use mariadb-connector-java
  instead.</p>

<p>* ckeditor3:
  See <a href="https://lists.debian.org/debian-lts/2022/05/msg00060.html">https://lists.debian.org/debian-lts/2022/05/msg00060.html</a> for
  further information.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:9+2022.06.02.</p>

<p>We recommend that you upgrade your debian-security-support packages.</p>

<p>For the detailed security status of debian-security-support please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/debian-security-support">https://security-tracker.debian.org/tracker/debian-security-support</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3038.data"
# $Id: $

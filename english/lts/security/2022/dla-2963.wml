<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata 2022a. Notable
changes are:</p>

<p>- Adjusted DST rules for Palestine, already in effect.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2021a-0+deb9u3.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>For the detailed security status of tzdata please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tzdata">https://security-tracker.debian.org/tracker/tzdata</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2963.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A heap-based buffer overflow flaw was found in libmodbus, a library for the
Modbus protocol, which can be abused for a denial of service attack or
memory corruption.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.1.4-2+deb10u2.</p>

<p>We recommend that you upgrade your libmodbus packages.</p>

<p>For the detailed security status of libmodbus please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libmodbus">https://security-tracker.debian.org/tracker/libmodbus</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3098.data"
# $Id: $

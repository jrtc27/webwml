<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was discovered in how p2p/p2p_pd.c in wpa_supplicant
before 2.10 processes P2P (Wi-Fi Direct) provision discovery requests.
It could result in denial of service or other impact (potentially
execution of arbitrary code), for an attacker within radio range.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2:2.4-1+deb9u9.</p>

<p>We recommend that you upgrade your wpa packages.</p>

<p>For the detailed security status of wpa please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wpa">https://security-tracker.debian.org/tracker/wpa</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2581.data"
# $Id: $

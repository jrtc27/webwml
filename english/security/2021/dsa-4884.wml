<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in ldb, a LDAP-like
embedded database built on top of TDB.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10730">CVE-2020-10730</a>

    <p>Andrew Bartlett discovered a NULL pointer dereference and
    use-after-free flaw when handling <q>ASQ</q> and <q>VLV</q> LDAP controls and
    combinations with the LDAP paged_results feature.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27840">CVE-2020-27840</a>

    <p>Douglas Bagnall discovered a heap corruption flaw via crafted
    DN strings.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20277">CVE-2021-20277</a>

    <p>Douglas Bagnall discovered an out-of-bounds read vulnerability in
    handling LDAP attributes that contains multiple consecutive
    leading spaces.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2:1.5.1+really1.4.6-3+deb10u1.</p>

<p>We recommend that you upgrade your ldb packages.</p>

<p>For the detailed security status of ldb please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/ldb">https://security-tracker.debian.org/tracker/ldb</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4884.data"
# $Id: $

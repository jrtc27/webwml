#use wml::debian::template title="Debian-Installer" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<h1>News</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Older news</a>
</p>

<h1>Installing with the Debian-Installer</h1>


<p>
<if-stable-release release="bullseye">
<strong>For official Debian <current_release_bullseye> installation media and
information</strong>, see
<a href="$(HOME)/releases/bullseye/debian-installer">the bullseye page</a>.
</if-stable-release>
<if-stable-release release="bookworm">
<strong>For official Debian <current_release_bookworm> installation media and
information</strong>, see
<a href="$(HOME)/releases/bookworm/debian-installer">the bookworm page</a>.
</if-stable-release>
</p>

<div class="tip">
<p>
All images linked below are for the version of Debian Installer being
developed for the next Debian release and will install Debian testing
(<q><current_testing_name></q>) by default.
</p>
</div>

<!-- Shown in the beginning of the release cycle: no Alpha/Beta/RC released yet. -->
<if-testing-installer released="no">
<p>

<strong>To install Debian testing</strong>, we recommend you use
the <strong>daily builds</strong> of the installer. The following images are available for
daily builds:

</p>

</if-testing-installer>

<!-- Shown later in the release cycle: Alpha/Beta/RC available, point at the latest one. -->
<if-testing-installer released="yes">
<p>

<strong>To install Debian testing</strong>, we recommend you use
the <strong><humanversion /></strong> release of the installer, after checking its
<a href="errata">errata</a>. The following images are available for
<humanversion />:

</p>

<h2>Official release</h2>

<div class="line">
<div class="item col50">
<strong>netinst (generally 180-450 MB) CD images</strong>
<netinst-images />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>other images (netboot, USB stick, etc.)</strong>
<other-images />
</div>
</div>

<p>
Or install the <b>current weekly snapshot of Debian testing</b>
which uses the same version of the installer as the last release:
</p>

<h2>Current weekly snapshots</h2>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>

<p>
If you prefer to use the latest and greatest, either to help us test a future
release of the installer or because of hardware problems or other issues,
try one of these <strong>daily built images</strong> which contain the latest
available version of installer components.
</p>
</if-testing-installer>

<h2>Current daily snapshots</h2>

<div class="line">
<div class="item col50">
<strong>netinst (generally 150-280 MB) <!-- and businesscard (generally 20-50 MB) --> CD images</strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>netinst <!-- and businesscard --> CD images (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>netinst multi-arch CD images</strong>
<devel-multi-arch-cd />
</div>

<div class="item col50 lastcol">
<strong>other images (netboot, USB stick, etc.)</strong>
<devel-other-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
If any of the hardware in your system <strong>requires non-free firmware to be
loaded</strong> with the device driver, you can use one of the
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/">\
tarballs of common firmware packages</a> or download an <strong>unofficial</strong> image
including these <strong>non-free</strong> firmwares. Instructions how to use the tarballs
and general information about loading firmware during an installation can
be found in the <a href="https://d-i.debian.org/doc/installation-guide/en.amd64/ch06s04.html">Installation Guide</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/daily-builds/sid_d-i/current/">unofficial
images with firmware included - daily builds</a>
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/weekly-builds/">unofficial
images with firmware included - weekly builds</a>

</p>
</div>

<hr />

<p>
<strong>Notes</strong>
</p>
<ul>
#	<li>Before you download the daily built images, we suggest you check for
#	<a href="https://wiki.debian.org/DebianInstaller/Today">known issues</a>.</li>
	<li>An architecture can be (temporarily) omitted from the overview of daily
	built images if daily builds are not (reliably) available.</li>
	<li>For the installation images, verification files
	(<tt>SHA512SUMS</tt> and <tt>SHA256SUMS</tt>) are available in the
	same directory as the images.</li>
	<li>For downloading full CD and DVD images the use of jigdo
	is recommended.</li>
	<li>Only a limited number of images from the full DVD sets are
	available as ISO files for direct download. Most users do not need
	all of the software available on all the discs, so to save space on
	download servers and mirrors the full sets are only available via
	jigdo.</li>
	<li>The multi-arch <em>netinst CD</em> image supports i386/amd64; the
	installation is similar to installing from a single architecture netinst
	image.</li>
</ul>

<p>
<strong>After using the Debian-Installer</strong>, please send us an
<a href="https://d-i.debian.org/manual/en.amd64/ch05s04.html#submit-bug">installation report</a>,
even if there weren't any problems.
</p>

<h1>Documentation</h1>

<p>
<strong>If you read only one document</strong> before installing, read our
<a href="https://d-i.debian.org/manual/en.amd64/apa.html">Installation
Howto</a>, a quick walkthrough of the installation process. Other useful
documentation includes:
</p>

<ul>
<li>Installation Guide:
#    <a href="$(HOME)/releases/stable/installmanual">version for current release</a>
#    &mdash;
    <a href="$(HOME)/releases/testing/installmanual">development version (testing)</a>
    &mdash;
    <a href="https://d-i.debian.org/manual/">latest version (Git)</a>
<br />
detailed installation instructions</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer FAQ</a>
and <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
common questions and answers</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
community maintained documentation</li>
</ul>

<h1>Contacting us</h1>

<p>
The <a href="https://lists.debian.org/debian-boot/">debian-boot
mailing list</a> is the main forum for discussion and work on
Debian-Installer.
</p>

<p>
We also have an IRC channel, #debian-boot on <tt>irc.debian.org</tt>. This
channel is used mainly for development, but occasionally for support.
If you do not receive a response, try the mailing list instead.
</p>

#use wml::debian::template title="Platform for Jonathan Carter" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"

<DIV>
<TABLE>
<TR><TD>
<BR>
<H1><BIG><B>Jonathan Carter</B></BIG><BR>
    <SMALL>DPL Platform</SMALL><BR>
    <SMALL>2022-03-14</SMALL>
</H1>
<H3>
<A HREF="mailto:jcc@debian.org"><TT>jcc@debian.org</TT></A><BR>
<A HREF="https://jonathancarter.org"><TT>https://jonathancarter.org</TT></A><BR>
<A HREF="https://wiki.debian.org/highvoltage"><TT>https://wiki.debian.org/highvoltage</TT></A>
</H3>
</TD></TR>
</TABLE>


<H2>1. Personal Introduction</H2>

<P>Hi, my name is Jonathan Carter, also known as <EM>highvoltage</EM>, with the Debian account name of <EM>jcc</EM>, and I'm running for a third DPL term. </P>

<P>I am a 40 year old Debian Developer who have various areas of interest within the Debian project. Like many other individuals and organisations
that contribute to Debian, I contribute to the project because I enjoy using Debian, and would like Debian to continue to exist and to flourish.
Debian remains unique in delivering a truly free distribution that also delivers stable releases and security updates, along with a very
large selection of software packages. Debian is an extremely important software project and its social contract adds to the project's uniqueness,
explicitly putting the priorities of our users first. We do not answer to shareholders or this quarter's bottom line, but instead we work together
to try to find the best solutions for our users. While this isn't always easy or straightforward, I find it very rewarding, and work that is worth doing.</P>


<H2>2. Why I'm running for DPL (again)</H2>

<P>
When I ran for my first DPL term, I was very nervous how it could all end up. Fortunately, two years later I've served two terms that I'm
quite proud of.
</p>

<P>
So why yet another term? Last year I learned that it's a lot harder pushing things forward in a release year, than in a non-release year.
During freeze, we're squarely focused on remaining issues to get the stable release out, and it's not the best time to have GRs or very
involved discussions about project changes. Shortly after our release, conversations were initiated to improve our voting process, followed
by another GR to make more voting changes. So for the next term, I would like to finish up (or at least make significant progress) on the items
that didn't progress over the last term. I'd also like to support some DDs I have spoken to about firmware changes in Debian. We have lots of
different positions on firmware within the project, but I also believe there is lots of common ground that we can build on, while at the same time
using our influence to positively shape the industry.
</P>


<H2>3. Agenda</H2>


<H3> 3.1. Formalise Debian, and some of our relationships and processes </H3>

<P><B>Consider formal registration of the Debian organisation.</B> I intend to initiate discussions on registering Debian as a formal organisation.
In 2020, Brian Gupta's DPL campaign largely revolved around founding a Foundation for the Debian project. I agree with many of his reasons for
wanting to pursue that, and want to propose a lighter version that. This is probably a good time to clarify that my DPL campaign
is neither a referendum on this matter and neither is this a campaign promise, I believe it's something that we need to seriously address together
as a project and make a decision based on its merits. Our lack of incorporation has brought many problems along with it. Including difficulties in setting up
agreements with external entities, and creating problems in terms of personal legal liability within the project. I believe that
a light approach that could address existing problems, while still preserving our relationships with our TOs who do important work for us,
is the best way forward.</P>

<P><B>Trusted organisations.</B> Currently we rely too much on various (and at times even conflicting) verbal agreements with our TOs. Once
we have Debian incorporated, and we're in a position to do so, we should implement some minimal agreements with our TOs that codifies our
relationships and responsibilities. Even if we incorporate Debian as its own organisation, TOs will still play a vital role, since the
services they provide is valuable and would be cumbersome to duplicate for no good reason.</P>

<P><B>Improve accounting.</B> Our treasurer team isn't properly empowered to do the job that we have assigned to them in their delegation. Tracking
Debian's assets is difficult, tedious and manual work. As a project, we don't know at any given time how much funds we have, and at times, working
with assets like trademarks have been a cause of frustration. Once we're in a position to make formal agreements with TOs, we can set some minimal
requirements and agreements on how we share responsibilities and accounting with our trusted organisations.</P>


<H3> 3.2. Technical </H3>

<P><B>Firmware.</B> The methods in which firmware is loaded and distributed has changed significantly over the last decades. Having non-updated
firmware and microcode can lead to significant security risks, and many new devices store no permanent embedded copies of firmware, requiring it to be
loaded from disk. This has some significant consequences for Debian. Our default free media doesn't ship with important microcode updates, and
on our live media we run into problems with both firmware and non-free drivers, causing a large amount of systems to be unusable with those media.
I'm not advocating to just include non-free bits on all our media, but I do think there are improvements we can make and actions we can take without
compromising our core values. I'd also like to approach both FSF, OSI and LF to see if there's scope for us to work together on firmware problems.
Also, we have quite a bit of funds available, we could make some funds available for the development of free firmware in the cases where it's plausible.</P>


<H2> 4. In Closing </H2>

<P>Thank you for taking the time to read my platform. Other than the agenda outlined above, I will continue to work towards making Debian a more stable,
welcoming project. I hope that the problems that we face in the world also improves, so that we can spend some more time together in person again.</P>

<!--=
<H2> A. Rebuttals </H2>

<H3> A.1. Felix Lechner </H3>

<H3> A.2. Hideki Yamane </H3>

<P>
</P>
-->

<H2> A. Changelog </H2>

<P> This platform is version controlled in a <a href="https://salsa.debian.org/jcc/dpl-platform">git repository.</a> </P>

<UL>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/4.0.0">4.0.0</a>: New platform for 2022 DPL elections.</LI>
</UL>

<BR>

</DIV>
